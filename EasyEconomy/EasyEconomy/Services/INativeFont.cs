﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEconomy.Services
{
    public interface INativeFont
    {
        float GetNativeSize(float size);
    }
}

﻿using EasyEconomy.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EasyEconomy.Services.Storage
{
    public interface IBusinessLogicQuery 
    {
        Task<List<EconomyDetail>> DetailByHeadIdsSortByDate(string ids);
        Task<List<EconomyDetail>> DetailBetweenDatesSortByDate(DateTime start, DateTime end);
        Task<double> TotalOut(string id);
        Task<double> TotalIn(string id);

        Task<double> TotalOut();
        Task<double> TotalIn();
    }
}

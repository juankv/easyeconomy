﻿using SQLite;

namespace EasyEconomy.Services.Storage
{
    public interface ISQLite
    {
        SQLiteAsyncConnection GetConnection();
        SQLiteAsyncConnection GetConnection(string dbName);
    }
}

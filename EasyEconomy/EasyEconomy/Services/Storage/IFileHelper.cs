﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEconomy.Services.Storage
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
        void CreateDataBaseFromAssets();
        string CreateDataBaseBackup();
        void RestoreDataBaseFromLastBackup();
    }
}

﻿using EasyEconomy.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EasyEconomy.Services.Storage
{
    //abstractos de los productos
    public interface IDetailDataStore<T, J>
    {
        Task<List<T>> GetItemsAsync(J ItemSearch, State state);
    }
}

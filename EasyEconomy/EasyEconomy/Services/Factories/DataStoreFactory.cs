﻿using EasyEconomy.Models;
using EasyEconomy.Services.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EasyEconomy.Services.Factories
{
    public abstract class AbstractDataStoreFactory
    {
        //el primer producto es para el manejo de la economia
        private List<object> _datastores = new List<object>();

        public AbstractDataStoreFactory()
        {
            this.CreateDataStores();
        }

        public List<object> DataStores
        {
            get { return _datastores; }
        }

        public abstract void CreateDataStores();       
    }

    public class EconomyDetailDataStores : AbstractDataStoreFactory
    {
        public override void CreateDataStores()
        {
            DataStores.Add(DependencyService.Get<IDataStore<CategoryDetail>>());
            DataStores.Add(DependencyService.Get<IDataStore<EconomyHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<EconomyDetail>>());
            DataStores.Add(DependencyService.Get<IDetailDataStore<EconomyDetail, EconomyHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<CategoryHead>>());
        }
    }

    public class CalendarDataStores : AbstractDataStoreFactory
    {
        public override void CreateDataStores()
        {
            DataStores.Add(DependencyService.Get<IDataStore<CategoryDetail>>());
            DataStores.Add(DependencyService.Get<IDataStore<EconomyHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<EconomyDetail>>());
            DataStores.Add(DependencyService.Get<IDetailDataStore<EconomyDetail, EconomyHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<CategoryHead>>());
        }
    }


    public class EconomyHeadDataStores : AbstractDataStoreFactory
    {

        public override void CreateDataStores()
        {
            DataStores.Add(DependencyService.Get<IDataStore<EconomyDetail>>());
            DataStores.Add(DependencyService.Get<IDataStore<EconomyHead>>());
            DataStores.Add(DependencyService.Get<IDetailDataStore<EconomyDetail, EconomyHead>>());
        }
    }

    public class CategoryHeadDataStores : AbstractDataStoreFactory
    {
        public override void CreateDataStores()
        {
            DataStores.Add(DependencyService.Get<IDetailDataStore<CategoryDetail, CategoryHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<CategoryHead>>());
            DataStores.Add(DependencyService.Get< IDataStore<CategoryDetail>>());
        }
    }

    public class EconomyDetailOperationDataStores : AbstractDataStoreFactory
    {
        public override void CreateDataStores()
        {
            DataStores.Add(DependencyService.Get<IDataStore<EconomyHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<EconomyDetail>>());
            DataStores.Add(DependencyService.Get<IDetailDataStore<EconomyDetail, EconomyHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<CategoryHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<CategoryDetail>>());
            DataStores.Add(DependencyService.Get<IDetailDataStore<CategoryDetail, CategoryHead>>());
        }
    }

    public class SubCategoryDataStores: AbstractDataStoreFactory
    {
        public override void CreateDataStores()
        {
            DataStores.Add(DependencyService.Get< IDataStore<CategoryHead>>());
            DataStores.Add(DependencyService.Get<IDetailDataStore<CategoryDetail, CategoryHead>>());
            DataStores.Add(DependencyService.Get<IDataStore<CategoryDetail>>());
        }
    }

    public class ChartsDataStores : AbstractDataStoreFactory
    {
        public override void CreateDataStores()
        {
            DataStores.Add(DependencyService.Get<IBusinessLogicQuery>());
            DataStores.Add(DependencyService.Get<IDataStore<EconomyDetail>>());
            DataStores.Add(DependencyService.Get<IDataStore<CategoryDetail>>());
            DataStores.Add(DependencyService.Get<IDataStore<CategoryHead>>());
        }
    }
}

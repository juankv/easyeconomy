﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using EasyEconomy.Models;
using Xamarin.Forms;
using System.Linq;
using EasyEconomy.Services.Storage;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    [QueryProperty(nameof(CategoryHeadId), nameof(CategoryHeadId))]
    public class UpdateCategoryViewModel : BaseViewModel
    {
        public IDataStore<CategoryHead> CategoryHeadStore => DependencyService.Get<IDataStore<CategoryHead>>();
        private string nombre;
        private string descripcion;
        private string categoryHeadId;
        

        public string CategoryHeadId
        {
            get => categoryHeadId;
            set
            {
                SetProperty(ref categoryHeadId, value);
                LoadCategoryId(value);
            }
        }

        public async void LoadCategoryId(string itemId)
        {
            try
            {
                var category = await CategoryHeadStore.GetItemAsync(itemId);
                Nombre = category.Nombre;
                Descripcion = category.Descripcion;
            }
            catch(Exception ex)
            {

            };
        }

        public UpdateCategoryViewModel()
        {
            Title = $"{AppResources.Update} {AppResources.Category}";
            UpdateCommand = new Command(OnUpdate, ValidateUpdate);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => UpdateCommand.ChangeCanExecute();
        }
        public string Nombre
        {
            get => nombre;
            set => SetProperty(ref nombre, value);
        }
        public string Descripcion
        {
            get => descripcion;
            set => SetProperty(ref descripcion, value);
        }
        public Command UpdateCommand { get; }
        public Command CancelCommand { get; }
        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
        private async void OnUpdate()
        {
            var id = categoryHeadId;
            var updateItem = await CategoryHeadStore.GetItemAsync(id);
            updateItem.Nombre = Nombre;
            updateItem.Descripcion = descripcion;
            updateItem.ModifiedDate = DateTime.Now;
            await CategoryHeadStore.UpdateItemAsync(updateItem);
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
        private bool ValidateUpdate()
        {
            return !String.IsNullOrWhiteSpace(nombre) 
                && !String.IsNullOrWhiteSpace(descripcion);
        }
    }
}

﻿using System;
using EasyEconomy.Models;
using Xamarin.Forms;
using System.Linq;
using EasyEconomy.Services.Storage;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    [QueryProperty(nameof(EconomyHeadId), nameof(EconomyHeadId))]
    public class NewEconomyDetailViewModel : EconomyDetailOperationViewModel
    {
        public NewEconomyDetailViewModel(): base()
        {
            Title = $"{AppResources.Add} {AppResources.Movement}";
            Tipos.Add(new PickerValue("+", AppResources.In));
            Tipos.Add(new PickerValue("-", AppResources.Out));            
            SaveCommand = new Command(OnSave, ValidateSave);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }


        public new void OnAppearing() 
        {
            base.OnAppearing();
            IsBusy = false;
        }


        public Command SaveCommand { get; }
        

        private async void OnSave()
        {
            EconomyDetail newItem = BuildNewEconomyDetail();
            await Factory.DataStores.OfType<IDataStore<EconomyDetail>>()
                .First().AddItemAsync(newItem);

            // This will pop the current page off the navigation stack
            await Shell.Current.
                GoToAsync($"..?{nameof(EconomyDetailViewModel.EconomyHeadId)}={EconomyHeadId}");
        }



    }
}

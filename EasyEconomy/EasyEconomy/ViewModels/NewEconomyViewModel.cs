﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using EasyEconomy.Models;
using EasyEconomy.Resx;
using Xamarin.Forms;

namespace EasyEconomy.ViewModels
{
    public class NewEconomyViewModel : EconomyViewModel
    {   
        public NewEconomyViewModel(): base()
        {
            Title = $"{AppResources.Add} {AppResources.Economy}";
            SaveCommand = new Command(OnSave, ValidateSave);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }
        public Command SaveCommand { get; }
        private async void OnSave()
        {            
            await EconomyHeadStore.AddItemAsync(BuildNewItem());
            await Shell.Current.GoToAsync("..");
        }
    }
}


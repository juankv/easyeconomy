﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using EasyEconomy.Models;
using EasyEconomy.Services.Storage;
using Xamarin.Forms;

namespace EasyEconomy.ViewModels
{
    public  class EconomyViewModel: BaseViewModel
    {

        public IDataStore<EconomyHead> EconomyHeadStore => DependencyService.Get<IDataStore<EconomyHead>>();
        private string nombre;
        private double presupuesto;
        private double excedente;
        private double deficid;
        private double montoFinal;
        private DateTime fechaCorte;
        private DateTime fechaCreacion;


        protected bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(nombre)
                && !Double.IsNaN(presupuesto)
                && !Double.IsNaN(excedente)
                && !Double.IsNaN(deficid)
                && !Double.IsNaN(montoFinal)
                && presupuesto != 0.0;
        }

        protected EconomyHead BuildNewItem()
        {
            FechaCreacion = DateTime.Now;
            FechaCorte = DateTime.Now;
            return new EconomyHead()
            {
                Nombre = Nombre,
                Presupuesto = Presupuesto,
                Excedente = Excedente,
                Deficid = Deficid,
                MontoFinal = MontoFinal,
                FechaCorte = FechaCorte,
                FechaCreacion = FechaCreacion,
                ModifiedDate = FechaCorte,
                State = State.Active
            };
        }

        protected async void UpdateItemHead(string economyHeadId)
        {
            var itemupdate = await EconomyHeadStore.GetItemAsync(economyHeadId);
            itemupdate.Nombre = Nombre;
            itemupdate.MontoFinal = MontoFinal;
            itemupdate.ModifiedDate = DateTime.Now;
            await EconomyHeadStore.UpdateItemAsync(itemupdate);
        }

        public string Nombre
        {
            get => nombre;
            set => SetProperty(ref nombre, value);
        }
        public double Presupuesto
        {
            get => presupuesto;
            set => SetProperty(ref presupuesto, value);
        }
        public double Excedente
        {
            get => excedente;
            set => SetProperty(ref excedente, value);
        }
        public double Deficid
        {
            get => deficid;
            set => SetProperty(ref deficid, value);
        }
        public double MontoFinal
        {
            get => montoFinal;
            set => SetProperty(ref montoFinal, value);
        }
        public DateTime FechaCorte
        {
            get => fechaCorte;
            set => SetProperty(ref fechaCorte, value);
        }
        public DateTime FechaCreacion
        {
            get => fechaCorte;
            set => SetProperty(ref fechaCreacion, value);
        }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        public EconomyViewModel()
        {
            excedente = 0.0;
            deficid = 0.0;
            montoFinal = 0.0;
            CancelCommand = new Command(OnCancel);
        }
        
    }
}

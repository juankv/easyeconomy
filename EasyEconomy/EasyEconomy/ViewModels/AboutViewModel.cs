﻿using Microcharts;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.Generic;
using SkiaSharp;
//Especificar que los entrys son los del nuget
using Entry = Microcharts.ChartEntry;
using EasyEconomy.Services;
using Rg.Plugins.Popup.Services;
using EasyEconomy.Popups;
using EasyEconomy.Services.Storage;
using System.Threading.Tasks;
using EasyEconomy.Helpers;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        Chart barChartInflowsOutflows;
        List<Entry> entryList;

        public Chart BarChartInflowsOutflows
        {
            get => barChartInflowsOutflows;
            set
            {
                if (barChartInflowsOutflows != value)
                {
                    barChartInflowsOutflows = value;
                    OnPropertyChanged();
                }
            }
        }

        public AboutViewModel()
        {            
            OpenChartsCommand = new Command( () => PopupNavigation.Instance.PushAsync(new ChartsPopupPage()));
            InitBarChart();
            SetBarChartTextSize();
        }


        public void InitBarChart()
        {
            entryList = new List<Entry>();
            barChartInflowsOutflows = new BarChart()
            {
                Entries = entryList
            };
        }

        public void SetBarChartTextSize()
        {
            var dependency = DependencyService.Get<INativeFont>();
            barChartInflowsOutflows.LabelTextSize = dependency.GetNativeSize(15);
        }

        public void OnAppearing()
        {
            var chartInOut = new ChartInOut();
            entryList.Add(chartInOut.GetEntryList()[0]);
            entryList.Add(chartInOut.GetEntryList()[1]);
            Title = AppResources.TitleAboutPage;
        }

        public void OnDisappearing()
        {
            entryList.Clear();
        }

        public ICommand OpenChartsCommand { get; }

    }

    public class ChartInOut
    {
        private IBusinessLogicQuery BusinessLogicQuery => DependencyService.Get<IBusinessLogicQuery>();
        private List<Entry> entryList;
        public ChartInOut(){
            SetEntryList();
        }

        public List<Entry> GetEntryList()
        {
            return entryList;
        }


        public async void SetEntryList()
        {
            entryList = new List<Entry>();
            entryList.Add(await In());
            entryList.Add(await Out());
        }
        
        public async Task<Entry> In()
        {
            var valueEntryOut = await BusinessLogicQuery.TotalIn();            
            return await Task.FromResult(GenerateEntry.NewEntry(AppResources.In, valueEntryOut, "#00bcd4"));
        }

        public async Task<Entry> Out()
        {
            var valueEntryOut = await BusinessLogicQuery.TotalOut();
            return await Task.FromResult(GenerateEntry.NewEntry(AppResources.Out, valueEntryOut, "#F44336"));
        }

    }
}
﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Input;
using EasyEconomy.Helpers;
using EasyEconomy.Models;
using EasyEconomy.Resx;
using Xamarin.Forms;

namespace EasyEconomy.ViewModels
{
    public class ChangeLanguageViewModel : BaseViewModel
    {

        public ObservableCollection<Language> Languages { get; set; }
        public Language SelectedLanguage { get; set; }

        public ICommand ChangeLangugeCommand { get; set; }

        public ChangeLanguageViewModel()
        {
            LoadLanguage();
            
            ChangeLangugeCommand = new Command(async () =>
            {
                LocalizationResourceManager.Instance.SetCulture(CultureInfo.GetCultureInfo(SelectedLanguage.CI));
                LoadLanguage();
                await App.Current.MainPage.DisplayAlert(AppResources.LanguageChanged, "", AppResources.Done);
                Title = AppResources.TitleLanguagesPage;
            });

        }

        internal void OnAppearing()
        {
            Title = AppResources.TitleLanguagesPage;
        }

        void LoadLanguage()
        {
            Languages = new ObservableCollection<Language>()
            {
                {new Language(AppResources.English, "en") },
                {new Language(AppResources.Spanish, "es") },
                {new Language(AppResources.French, "fr") }
            };
            SelectedLanguage = Languages.FirstOrDefault(pro => pro.CI == LocalizationResourceManager.Instance.CurrentCulture.TwoLetterISOLanguageName);
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

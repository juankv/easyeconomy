﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyEconomy.Models;
using EasyEconomy.Services.Factories;
using EasyEconomy.Services.Storage;
using EasyEconomy.Views;
using Xamarin.Forms;

namespace EasyEconomy.ViewModels
{
    public class EconomyDetailOperationViewModel : BaseViewModel
    {

        private string nombre;
        private string presupuesto;
        private string fechaCreacion;
        private string economyHeadId;
        private double amount;
        private PickerValue typeSelected;
        private PickerValue selectedCategory;
        private PickerValue selectedSubCategory;
        public ObservableCollection<PickerValue> Tipos { get; }
        public ObservableCollection<PickerValue> Categories { get; }
        public ObservableCollection<PickerValue> SubCategories { get; }

        public ObservableCollection<PickerValue> Economies { get; }


        private PickerValue selectedEconomy;

        public EconomyDetailOperationViewModel()
        {
            Tipos = new ObservableCollection<PickerValue>();
            Economies = new ObservableCollection<PickerValue>();
            Categories = new ObservableCollection<PickerValue>();
            SubCategories = new ObservableCollection<PickerValue>();
            CancelCommand = new Command(OnCancel);
            Factory = new EconomyDetailOperationDataStores();
        }

        public void OnAppearing()
        {

            TypeSelected = null;
            selectedCategory = null;
            selectedSubCategory = null;
            GetEconomyHead();
            IsBusy = false;
        }

        public PickerValue SelectedEconomy
        {
            get => selectedEconomy;
            set
            {
                if (selectedEconomy != value)
                {
                    selectedEconomy = value;
                    OnPropertyChanged();
                    OnEconomyPickerSelectedIndexChanged(value);
                }
            }
        }
        public string EconomyHeadId
        {
            get
            {
                return economyHeadId;
            }
            set
            {
                if (economyHeadId != value)
                {
                    economyHeadId = value;
                    LoadItemId(value);
                }
            }
        }

        public string Nombre
        {
            get => nombre;
            set => SetProperty(ref nombre, value);
        }

        public string Presupuesto
        {
            get => presupuesto;
            set => SetProperty(ref presupuesto, value);
        }

        public string FechaCreacion
        {
            get => fechaCreacion;
            set => SetProperty(ref fechaCreacion, value);
        }
        


        public double Amount
        {
            get => amount;
            set => SetProperty(ref amount, value);
        }

        public async void LoadItemId(string itemId)
        {
            try
            {
                var economyHead = await Factory.DataStores.OfType<IDataStore<EconomyHead>>().First().GetItemAsync(itemId);
                economyHeadId = economyHead.EconomyHeadId.ToString();
                Nombre = economyHead.Nombre;
                Presupuesto = economyHead.Presupuesto.ToString("C0");
                FechaCreacion = economyHead.
                    FechaCreacion.ToString("yyyy-MM-dd HH:mm:ss");
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }

        void OnEconomyPickerSelectedIndexChanged(object sender)
        {
            var economy = (PickerValue)sender;
            App.EconomyId = int.Parse(economy.Id);
            EconomyHeadId = economy.Id;
        }


        private async void OnCancel()
        {
            await Shell.Current.GoToAsync($"..?{nameof(NewEconomyDetailViewModel.EconomyHeadId)}={EconomyHeadId}");
        }


        protected bool ValidateSave()
        {
            var withOutCategorySubcategory = Amount != 0 && !double.IsNaN(Amount) && TypeSelected != null;
            var withCategorySubcategory = withOutCategorySubcategory && SelectedCategory != null && SelectedSubCategory != null;
            return withOutCategorySubcategory || withCategorySubcategory;
        }

        public async void GetEconomyHead()
        {
            var headEconomies = await Factory.DataStores.
                OfType<IDataStore<EconomyHead>>().
                First().GetItemsByStateAsync(State.Active);
            Economies.Clear();
            var count = 0;
            foreach (var economyHead in headEconomies)
            {
                Economies.Add(new PickerValue(economyHead.EconomyHeadId +
                    "", economyHead.Nombre + "-" + economyHead.Presupuesto.ToString("C0")));

                if (App.EconomyId != 0 && economyHead.EconomyHeadId == App.EconomyId)
                {
                    break;
                }
                count++;
            }

            if (Economies.Count > 0 && App.EconomyId != 0)
                SelectedEconomy = Economies[count];
        }

        public PickerValue TypeSelected
        {
            get { return typeSelected; }
            set
            {
                if (TypeSelected != value)
                {
                    typeSelected = value;
                    OnPropertyChanged();
                    OnTipoPickerSelectedIndexChanged(value);
                }
            }
        }

        public PickerValue SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                if (selectedCategory != value)
                {
                    selectedCategory = value;
                    OnPropertyChanged();
                    OnPickerSelectedIndexChanged(value);
                }
            }
        }

        public async void OnTipoPickerSelectedIndexChanged(object sender)
        {
            Categories.Clear();
            selectedCategory = null;
            SubCategories.Clear();
            selectedSubCategory = null;
            if (sender == null)
            {
                return ;
            }
            var type = (PickerValue)sender;
            var categories = await Factory.DataStores.OfType<IDataStore<CategoryHead>>().First().GetItemsByStateAsync(State.Active);
            foreach (var category in categories)
            {
                Categories.Add(new PickerValue(category.CategoryHeadId.ToString(), category.Nombre));
            }

        }


        async void OnPickerSelectedIndexChanged(object sender)
        {
            selectedSubCategory = null;
            SubCategories.Clear();
            if(sender == null)
            {
                return;
            }
            var category = (PickerValue)sender;
            var categorySeach = await Factory.DataStores.
                OfType<IDataStore<CategoryHead>>().First().
                GetItemAsync(category.Id);
            var subcategories = await Factory.
                DataStores.
                OfType<IDetailDataStore<CategoryDetail, CategoryHead>>().
                First().GetItemsAsync(categorySeach, State.Active);
            SubCategories.Clear();
            foreach (var subcategory in subcategories)
            {
                SubCategories.Add(new PickerValue(subcategory.
                    CategoryDetailId.
                    ToString(), subcategory.Nombre));
            }

        }

        public PickerValue SelectedSubCategory
        {
            get { return selectedSubCategory; }
            set
            {
                if (selectedSubCategory != value)
                {
                    selectedSubCategory = value;
                    OnPropertyChanged();
                }
            }
        }


        public Command CancelCommand { get; }


        protected EconomyDetail BuildNewEconomyDetail()
        {
            var id = int.Parse(EconomyHeadId);
            var datetimenow = DateTime.Now;
            EconomyDetail newItem = null;

            var idDetail = int.Parse(selectedSubCategory.Id);

            newItem = new EconomyDetail()
            {
                CreateDate = datetimenow,
                ModifiedDate = datetimenow,
                Kind = TypeSelected.Id,
                Amount =   Amount,
                CategoryDetailId = idDetail,
                State = State.Active,
                EconomyHeadId = id
            };

            return newItem;
        }

        protected async Task<EconomyDetail> BuildUpdateEconomyDetail(string EconomyDetailId)
        {
            EconomyDetail updateItem = await Factory.DataStores.OfType<IDataStore<EconomyDetail>>().First().GetItemAsync(EconomyDetailId);
            var datetimenow = DateTime.Now;
            int idDetail = int.Parse(selectedSubCategory.Id);
            int economyHeadId = int.Parse(EconomyHeadId);
            updateItem.ModifiedDate = datetimenow;
            updateItem.Kind = TypeSelected.Id;
            updateItem.Amount = Amount;
            updateItem.CategoryDetailId = idDetail;
            updateItem.EconomyHeadId = economyHeadId;

            return await Task.FromResult(updateItem);
        }
    }
}

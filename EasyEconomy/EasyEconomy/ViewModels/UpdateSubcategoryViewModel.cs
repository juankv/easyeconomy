﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using EasyEconomy.Models;
using EasyEconomy.Resx;
using EasyEconomy.Services.Storage;
using EasyEconomy.Views;
using Xamarin.Forms;

namespace EasyEconomy.ViewModels
{
    [QueryProperty(nameof(CategoryHeadId), nameof(CategoryHeadId))]
    [QueryProperty(nameof(SubCategoryId), nameof(SubCategoryId))]
    public class UpdateSubcategoryViewModel : BaseViewModel
    {
        public IDataStore<CategoryDetail> CategoryDetailStore => DependencyService.Get<IDataStore<CategoryDetail>>();
        private string nombre;
        private string descripcion;
        private string categoryHeadId;
        private string subCategoryId;

        public string Nombre
        {
            get => nombre;
            set => SetProperty(ref nombre, value);
        }

        public string Descripcion
        {
            get => descripcion;
            set => SetProperty(ref descripcion, value);
        }


        public string CategoryHeadId
        {
            get => categoryHeadId;
            set => SetProperty(ref categoryHeadId, value);
        }

        public string SubCategoryId
        {
            get => subCategoryId;
            set
            {
                subCategoryId = value;
                LoadDetailId(value);
            }
        }

        public async void LoadDetailId(string itemId)
        {
            var itemupdate = await CategoryDetailStore.GetItemAsync(itemId);
            Nombre = itemupdate.Nombre;
            Descripcion = itemupdate.Descripcion;
        }

        public UpdateSubcategoryViewModel()
        {
            Title = $"{AppResources.Update} {AppResources.SubCategory}";
            UpdateCommand = new Command(OnUpdate, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => UpdateCommand.ChangeCanExecute();
        }

        public Command UpdateCommand { get; }
        public Command CancelCommand { get; }


        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");

        }

        private async void OnUpdate()
        {
            var id = SubCategoryId;
            var intCategoryHeadId = int.Parse(CategoryHeadId);
            var updateitem = await CategoryDetailStore.GetItemAsync(id);

            updateitem.CategoryHeadId = intCategoryHeadId;
            updateitem.Nombre = Nombre;
            updateitem.Descripcion = Descripcion;
            updateitem.ModifiedDate = DateTime.Now;

            await CategoryDetailStore.UpdateItemAsync(updateitem);
            Shell.Current.IsBusy = false;
            await Shell.Current.GoToAsync($"..?{nameof(SubCategoryViewModel.CategoryHeadId)}={CategoryHeadId}");
        }


        private bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(nombre)
                && !String.IsNullOrWhiteSpace(descripcion);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using EasyEconomy.Models;
using EasyEconomy.Views;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;
using EasyEconomy.Services.Storage;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    [QueryProperty(nameof(EconomyHeadId), nameof(EconomyHeadId))]
    [QueryProperty(nameof(EconomyDetailId), nameof(EconomyDetailId))]
    public class UpdateEconomyDetailViewModel : EconomyDetailOperationViewModel
    {
        private string economyDetailId;
        public UpdateEconomyDetailViewModel() : base()
        {
            Title = $"{AppResources.Update} {AppResources.Movement}";
            Tipos.Add(new PickerValue("+", AppResources.In));
            Tipos.Add(new PickerValue("-", AppResources.Out));
            UpdateCommand = new Command(OnUpdate, ValidateSave);
            this.PropertyChanged +=
                (_, __) => UpdateCommand.ChangeCanExecute();
        }
        public string EconomyDetailId
        {
            get
            {
                return economyDetailId;
            }
            set
            {
                if(value != economyDetailId) { 
                    economyDetailId = value;
                    LoadDetailId(value);
                }
            }
        }
        public new void OnAppearing()
        {
            base.OnAppearing();
            IsBusy = false;
        }
        public Command UpdateCommand { get; }
        public async void LoadDetailId(string itemId)
        {
            try
            {
                var economyDetail = await Factory.DataStores.OfType<IDataStore<EconomyDetail>>().First().GetItemAsync(itemId);
                SelectedEconomy = Economies.FirstOrDefault(x => x.Id == economyDetail.EconomyHeadId.ToString());
                economyDetailId = economyDetail.EconomyDetailId.ToString();
                Amount = economyDetail.Amount;
                await Task.Delay(50);
                TypeSelected = Tipos.FirstOrDefault(x => x.Id == economyDetail.Kind);
                await Task.Delay(50);
                var subCategory = await Factory.DataStores.OfType<IDataStore<CategoryDetail>>().First().GetItemAsync(economyDetail.CategoryDetailId.ToString());
                var category = await Factory.DataStores.OfType<IDataStore<CategoryHead>>().First().GetItemAsync(subCategory.CategoryHeadId.ToString());
                SelectedCategory = Categories.FirstOrDefault(y => y.Id == category.CategoryHeadId.ToString());
                await Task.Delay(70);
                SelectedSubCategory = SubCategories.FirstOrDefault(x => x.Id == subCategory.CategoryDetailId.ToString());

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
        private async void OnUpdate()
        {
            var economyDetailId = EconomyDetailId;
            var updateItem = await BuildUpdateEconomyDetail(economyDetailId);
            await Factory.DataStores.OfType<IDataStore<EconomyDetail>>().First().UpdateItemAsync(updateItem);
            await Shell.Current.
                GoToAsync($"..?{nameof(EconomyDetailViewModel.EconomyHeadId)}={EconomyHeadId}");
        }

    }
}

﻿using Microcharts;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.Generic;
using SkiaSharp;
using Entry = Microcharts.ChartEntry;
using EasyEconomy.Services;
using Rg.Plugins.Popup.Services;
using EasyEconomy.Popups;
using EasyEconomy.Services.Storage;
using EasyEconomy.Models;
using System.Linq;
using EasyEconomy.Services.Factories;
using System;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    public class ChartsViewModel :  BaseViewModel
    {
        private Chart barChart;
        private string titleChartPopup;
        private string descriptionChartPopup;
        private ObservableCollection<Entry> entryList;
        private Director director ;
        private BuilderDataCharts builder;
        private Product product;
        public Chart BarChart
        {
            get => barChart;
            set
            {
                if (barChart != value)
                {
                    barChart = value;
                    OnPropertyChanged();
                }
            }
        }

        public ICommand CategoryChartCommand { get; }
        public ICommand SubCategoryChartCommand { get; }

        public string TitleChartPopup
        {
            get => titleChartPopup;
            set
            {
                if (titleChartPopup != value)
                {
                    titleChartPopup = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DescriptionChartPopup
        {
            get => descriptionChartPopup;
            set
            {
                if (descriptionChartPopup != value)
                {
                    descriptionChartPopup = value;
                    OnPropertyChanged();
                }
            }
        }

        public ChartsViewModel()
        {
            
            CategoryChartCommand = new Command(async () => await GenerateEntriesCategories());
            SubCategoryChartCommand = new Command(async () => await GenerateEntriesSubCategories());
            
            InitBuilderPattern();
            InitBarChartSubCategories();
            SetBarChartTextSize();
        }

        public void InitBuilderPattern()
        {
            Factory = new ChartsDataStores();
            director = new Director();
            builder = new BuilderDataCharts();
            product = new Product();
        }

        public void SetBarChartTextSize()
        {
            var dependency = DependencyService.Get<INativeFont>();
            barChart.LabelTextSize = dependency.GetNativeSize(15);
        }


        public void InitBarChartSubCategories()
        {
            entryList = new ObservableCollection<Entry>();
            barChart = new LineChart()
            {
                Entries = entryList
            };
        }


        public void OnAppearing()
        {
            GenerateEntriesCategories();
        }

        
        public async Task<bool> GenerateEntriesCategories()
        {
            entryList.Clear();

            TitleChartPopup = AppResources.ChartByCategoryTitle;
            DescriptionChartPopup = AppResources.ChartByCategoryDesciption;
            await GenerateData(OwnChartType.Categories);

            var chartCategories = GenerateGroupByCategories();

            SetEntryList(chartCategories.GetEntryList());


            BarChart = new  BarChart()
            {
                Entries = entryList
            };

            SetBarChartTextSize();

            return await Task.FromResult(true);
        }

        public async Task<bool> GenerateEntriesSubCategories()
        {
            entryList.Clear();
            TitleChartPopup = AppResources.ChartBySubCategoryTitle;
            DescriptionChartPopup = AppResources.ChartBySubCategoryDesciption;
            await GenerateData(OwnChartType.SubCategories);

            var chartSubCategories = new 
                ChartSubCategories(EconomyDetailGroupBySubCategory().Take(10));

            SetEntryList(chartSubCategories.GetEntryList());

            if (entryList.Count > 2)
                BarChart = new LineChart()
                {
                    Entries = entryList
                };
            else
                BarChart = new BarChart()
                {
                    Entries = entryList
                };

            SetBarChartTextSize();

            return await Task.FromResult(true);
        }

        public void SetEntryList(List<Entry> entries)
        {
            foreach (var entry in entries)
            {
                entryList.Add(entry);
            }
        }

       public async Task<bool> GenerateData(OwnChartType chartType)
        {
            this.director.Builder = this.builder;
            switch (chartType)
            {
                case OwnChartType.Categories:
                    await this.director.BuildCategories(this.Factory);
                    break;
                case OwnChartType.SubCategories:
                    await this.director.BuildSubCategories(Factory);
                    break;
            }
            this.product = this.builder.GetProduct();
            return await Task.FromResult(true);
        }


        public ChartCategories GenerateGroupByCategories()
        {
            var economyDetailGroupBySubCategory = EconomyDetailGroupBySubCategory();
            var economyDetailGroupByCategory = EconomyDetailGroupByCategory(economyDetailGroupBySubCategory);

            return  new
                ChartCategories(economyDetailGroupByCategory);
        }


        public IEnumerable<ChartSubCategoryInfo> EconomyDetailGroupBySubCategory()
        {
            return from p in product.GetEconomyDetail()
                   group p by p.CategoryDetailId into pg
                   // join *after* group
                   join bp in product.GetCategoryDetail() on pg.FirstOrDefault().CategoryDetailId equals bp.CategoryDetailId
                   
                   select new ChartSubCategoryInfo()
                   {
                       Id = pg.FirstOrDefault().CategoryDetailId,
                       Id2 = bp.CategoryHeadId,
                       Amount = pg.Sum(m => m.Amount),
                       Name =  bp.Nombre  
                   };
        }

        public IEnumerable<ChartCategoryInfo> EconomyDetailGroupByCategory(IEnumerable<ChartSubCategoryInfo> subCategoryInfo)
        {
            
            return  from a in subCategoryInfo
                    join b in product.GetCategoryHead() on a.Id2 equals b.CategoryHeadId
                                 group new { b.CategoryHeadId, b.Nombre, a.Amount }
                                 by b.CategoryHeadId into pg
                                 select new ChartCategoryInfo()
                                 {
                                     Id = pg.First().CategoryHeadId,
                                     Name = pg.First().Nombre,
                                     Amount = pg.Sum(m => m.Amount)
                                 };
        }
        public void OnDisappearing()
        {
            entryList.Clear();
        }
    }

    public enum OwnChartType
    {
        Categories,
        SubCategories
    }

    public class Director
    {
        private IBuilderDataCharts _builder;

        public IBuilderDataCharts Builder
        {
            set { _builder = value; }
        }

        // The Director can construct several product variations using the same
        // building steps.
        public async Task<bool> BuildSubCategories(AbstractDataStoreFactory Factory2)
        {
            var IsBuiltSubCategories = await this._builder.BuildSubCategories(Factory2);
            var IsBuiltEconomiesDetail = await this._builder.BuildEconomiesDetail(Factory2);
            return await Task.FromResult(IsBuiltSubCategories  && IsBuiltEconomiesDetail);
        }

        public async Task<bool> BuildCategories(AbstractDataStoreFactory Factory2)
        {
            var IsBuiltSubCategories = await this._builder.BuildSubCategories(Factory2);
            var IsBuiltCategories = await this._builder.BuildCategories(Factory2);
            var IsBuiltEconomiesDetail = await this._builder.BuildEconomiesDetail(Factory2);
            return await Task.FromResult(IsBuiltSubCategories && IsBuiltCategories && IsBuiltEconomiesDetail);
        }

    }

    public interface IBuilderDataCharts
    {
        Task<bool> BuildEconomiesDetail(AbstractDataStoreFactory Factory);

        Task<bool> BuildSubCategories(AbstractDataStoreFactory Factory);

        Task<bool> BuildCategories(AbstractDataStoreFactory Factory);
    }

    public class BuilderDataCharts : IBuilderDataCharts
    {
        private Product _product = new Product();
        public BuilderDataCharts()
        {
            this.Reset();
        }

        public void Reset()
        {
            this._product = new Product();
        }
        public async Task<bool> BuildCategories(AbstractDataStoreFactory Factory)
        {
            var categories = await Factory.DataStores.
                OfType<IDataStore<CategoryHead>>().
                First().GetItemsByStateAsync(State.Active);
            this._product.Add(categories);
            return await Task.FromResult(true);
        }

        public async Task<bool> BuildEconomiesDetail(AbstractDataStoreFactory Factory)
        {
            var economiesDetail = await Factory.DataStores.
                OfType<IDataStore<EconomyDetail>>().
                First().GetItemsByStateAsync(State.Active);
            this._product.Add(economiesDetail);
            return await Task.FromResult(true);
        }

        public async Task<bool> BuildSubCategories(AbstractDataStoreFactory Factory)
        {
            var categoriesDetail = await Factory.DataStores.
               OfType<IDataStore<CategoryDetail>>().
               First().GetItemsByStateAsync(State.Active);
            this._product.Add(categoriesDetail);
            return await Task.FromResult(true);
        }

     
        public Product GetProduct()
        {
            Product result = this._product;

            this.Reset();

            return result;
        }
    }

    public class Product
    {

        private List<object> _parts = new List<object>();
        
        public void Add(object part)
        {
            this._parts.Add(part);
        }

        public  IEnumerable<EconomyDetail> GetEconomyDetail()
        {
            return this._parts.OfType<IEnumerable<EconomyDetail>>().First();
        }
        public IEnumerable<CategoryDetail> GetCategoryDetail()
        {
            return this._parts.OfType<IEnumerable<CategoryDetail>>().First();
        }

        public IEnumerable<CategoryHead> GetCategoryHead()
        {
            return this._parts.OfType<IEnumerable<CategoryHead>>().First();
        }
    }

    public class ChartSubCategories
    {
        private List<Entry> entryList;
        public ChartSubCategories(IEnumerable<ChartSubCategoryInfo> AmongBySubCategory)
        {
            SetEntryList(AmongBySubCategory);
        }

        public List<Entry> GetEntryList()
        {
            return entryList;
        }


        public void SetEntryList(IEnumerable<ChartSubCategoryInfo> AmongBySubCategory)
        {
            entryList = new List<Entry>();
            foreach (var categoryDetailAmount in AmongBySubCategory.OrderByDescending(x => x.Amount))
            {
                entryList.Add(GenerateEntry.NewEntry(categoryDetailAmount.Name, categoryDetailAmount.Amount));
            }
        }
    }


    public class ChartCategories
    {
        private List<Entry> entryList;
        public ChartCategories(IEnumerable<ChartCategoryInfo> AmongByCategory)
        {
            SetEntryList(AmongByCategory);
        }
        public List<Entry> GetEntryList()
        {
            return entryList;
        }
        public void SetEntryList(IEnumerable<ChartCategoryInfo> AmongByCategory)
        {
            entryList = new List<Entry>();
            foreach (var categoryDetailAmount in AmongByCategory.OrderByDescending(x => x.Amount))
            {
                entryList.Add(GenerateEntry.NewEntry(categoryDetailAmount.Name, categoryDetailAmount.Amount));
            }
        }
    }

    public static class GenerateEntry
    {
        public static Entry NewEntry(string label, double value)
        {
            return new Entry(float.Parse(value.ToString()))
            {
                Label = label,
                ValueLabel = value.ToString("C0"),
                Color = SKColor.Parse(GenerateColor.GetHex())
            };
        }

        public static Entry NewEntry(string label, double value, string color)
        {
            return new Entry(float.Parse(value.ToString()))
            {
                Label = label,
                ValueLabel = value.ToString("C0"),
                Color = SKColor.Parse(color)
            };
        }
    }

    public static class GenerateColor
    {
        public static string GetHex()
        {
            return GetHexString(RandomColor());
        }
        public static Color RandomColor()
        {
            Random rand = new Random();
            int max = byte.MaxValue + 1; // 256
            int r = rand.Next(max);
            int g = rand.Next(max);
            int b = rand.Next(max);
            return Color.FromRgb(r, g, b);
        }
        public static string GetHexString(this Xamarin.Forms.Color color)
        {
            var red = (int)(color.R * 255);
            var green = (int)(color.G * 255);
            var blue = (int)(color.B * 255);
            var alpha = (int)(color.A * 255);
            var hex = $"#{alpha:X2}{red:X2}{green:X2}{blue:X2}";
            return hex;
        }
    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using EasyEconomy.Models;
using EasyEconomy.Views;
using System.Windows.Input;
using System.Linq;
using EasyEconomy.Services.Storage;
using EasyEconomy.Services.Factories;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    public class CategoryHeadViewModel : BaseViewModel
    {

        private CategoryHeadView _selectedItem;
        public ObservableCollection<CategoryHeadView> Items { get; }
        
        public Command AddCategoryHeadCommand { get; }
        public CategoryHeadViewModel()
        {
            
            Items = new ObservableCollection<CategoryHeadView>();
            AddCategoryHeadCommand = new Command(OnAddCategoryHead);
            Factory = new CategoryHeadDataStores();
        }
        async Task ExecuteLoadCategoryHeadCommand()
        {            
            try
            {
                Items.Clear();
                var items = await Factory.DataStores.
                    OfType<IDataStore<CategoryHead>>().First()
                    .GetItemsByStateAsync(State.Active);
                foreach (var item in items)
                {
                    Items.Add(new CategoryHeadView(item));
                }                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            IsBusy = false;
            AppShell.Current.IsBusy = false;
        }
        public void OnAppearing()
        {
            Title = AppResources.TitleCategoriesPage;
            SelectedItem = null;
            IsBusy = false;
            ExecuteLoadCategoryHeadCommand();
        }
        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetProperty(ref _isRefreshing, value); }
        }
        public CategoryHeadView SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
            }
        }
        public ICommand ItemTappedCommand => new Command(async (item) => await OnItemTappedCommandExecuted(item));
        private async Task OnItemTappedCommandExecuted(object item)
        {
            IsBusy = true;
            if (item == null)
                return;
            var order = item as Xamarin.Forms.ItemTappedEventArgs;
            var itemSelect = order.Item as CategoryHeadView;
            App.CategoryId = itemSelect.CategoryHeadId;
            await Shell.Current.GoToAsync(
                $"{nameof(SubCategoryPage)}?{nameof(SubCategoryViewModel.CategoryHeadId)}={itemSelect.CategoryHeadId}"
                );            
            IsBusy = false;
        }
         private async void OnAddCategoryHead(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewCategoryPage));
            IsBusy = false;
        }

        /* async void OnEconomyHeadSelected(EconomyHeadView item)
         {
             if (item == null)
                 return;

             // This will push the ItemDetailPage onto the navigation stack
             await Shell.Current.GoToAsync($"{nameof(ItemDetailPage)}?{nameof(ItemDetailViewModel.ItemId)}={item.Id}");
         }*/

        public async Task ExecuteEditCategoryCommand(string idCategory)
        {
            await Shell.Current.GoToAsync($"{nameof(UpdateCategoryPage)}?CategoryHeadId={idCategory}");
        }

        public async Task ExecuteDeleteCategoryCommand(string idCategory)
        {
            var categoryHead = await Factory.DataStores.OfType<IDataStore<CategoryHead>>().First().GetItemAsync(idCategory);
            var deleteDetatils = await Factory.DataStores.
                OfType<IDetailDataStore<CategoryDetail, CategoryHead>>().
                First().GetItemsAsync(categoryHead, State.Active);
            var answer = await Application.Current.MainPage.DisplayAlert($"{AppResources.Delete} {AppResources.Category}", 
                string.Format(AppResources.ConfirmDeleteCategory  , categoryHead.Nombre, deleteDetatils.Count())
                , AppResources.Yes, AppResources.No);
            if (!answer) return;

            foreach (var itemdeletedeatil in deleteDetatils)
            {
                await Factory.DataStores.OfType<IDataStore<CategoryDetail>>().First().DeleteItemAsync(itemdeletedeatil.CategoryDetailId.ToString());
            }

            Items.Remove(Items.Where(x => x.CategoryHeadId == idCategory).FirstOrDefault());

            await Factory.DataStores.OfType<IDataStore<CategoryHead>>().First().DeleteItemAsync(idCategory);
        }
    }

}

﻿using EasyEconomy.Models;
using EasyEconomy.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using EasyEconomy.Services.Storage;
using EasyEconomy.Services.Factories;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    [QueryProperty(nameof(CategoryHeadId), nameof(CategoryHeadId))]
    public class SubCategoryViewModel : BaseViewModel
    {
        private string categoryHeadId;
        private string nombre;
        private string tipo;
        private string descripcion;
        private SubCategoryView _selectedItem;
        public SubCategoryView SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
            }
        }

        public ObservableCollection<SubCategoryView> Items { get; }

        public Command AddSubCategoryCommand { get; }
        public string Nombre
        {
            get => nombre;
            set => SetProperty(ref nombre, value);
        }

        public string Tipo
        {
            get => tipo;
            set => SetProperty(ref tipo, value);
        }

        public string Descripcion
        {
            get => descripcion;
            set => SetProperty(ref descripcion, value);
        }

        public void OnAppearing()
        {
            IsBusy = false;
        }

        public string CategoryHeadId
        {
            get
            {
                return categoryHeadId;
            }
            set
            {
                categoryHeadId = value;
                LoadItemId(value);
            }
        }

        private PickerValue selectedTipo;
        public PickerValue TypeSelected
        {
            get { return selectedTipo; }
            set
            {
                if (selectedTipo != value)
                {
                    selectedTipo = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<PickerValue> Tipos { get; }
        public SubCategoryViewModel()
        {
            Items = new ObservableCollection<SubCategoryView>();
            AddSubCategoryCommand = new Command(OnAddSubCategory);
            Factory = new SubCategoryDataStores();
        }        

        private async void OnAddSubCategory(object obj)
        {
            IsBusy = false;
            await Shell.Current.GoToAsync($"{nameof(NewSubCategoryPage)}?{nameof(NewSubCategoryViewModel.CategoryHeadId)}={categoryHeadId}");
        }

        public async void LoadItemId(string itemId)
        
        {
            if (IsBusy)
                return;
            try
            {
                Items.Clear();
                IsBusy = true;
                await FillLabelInfo();
                await BuildListView();
                IsBusy = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed to Load Item");
            }
            finally
            {
                IsBusy = false;
            }
        }

        public int GetCategoryHeadId()
        {
            if (CategoryHeadId == null)
                return  int.Parse(App.CategoryId);
            else
                return int.Parse(CategoryHeadId);
        } 

        public async Task FillLabelInfo()
        {
            var categoryHead = await GetCategoryHead();
            Nombre = categoryHead.Nombre;
            Descripcion = AppResources.Description + ": " + 
                char.ToUpper(categoryHead.Descripcion[0]) + categoryHead.Descripcion.Substring(1);
            Title = categoryHead.Nombre +"-"+ AppResources.TitleSubCategoriesPage;
        }

        async Task<CategoryHead> GetCategoryHead()
        {
            var categoryheadId = GetCategoryHeadId();
            return await Factory.DataStores.OfType<IDataStore<CategoryHead>>().First().GetItemAsync(categoryheadId.ToString());
        } 

        async Task BuildListView()
        {
            var categoryHead = await GetCategoryHead();
            var categoryDetail = await Factory.DataStores.
                OfType<IDetailDataStore<CategoryDetail, CategoryHead>>().
                First().GetItemsAsync(categoryHead, State.Active);
            foreach (var item in categoryDetail)
            {
                Items.Add(new SubCategoryView(item));
            }
        }

        public async Task ExecuteEditSubCategoryCommand(string idSubCategory)
        {
            await Shell.Current.GoToAsync($"{nameof(UpdateSubCategoryPage)}?{nameof(UpdateSubcategoryViewModel.CategoryHeadId)}={categoryHeadId}&{nameof(UpdateSubcategoryViewModel.SubCategoryId)}={idSubCategory}");
        }

        public async Task ExecuteDeleteSubCategoryCommand(string idSubCategory)
        {
            var categoryDetail = await Factory.DataStores.OfType<IDataStore<CategoryDetail>>().First().GetItemAsync(idSubCategory);
            var answer = await Application.Current.MainPage.DisplayAlert(
                $"{AppResources.Delete} {AppResources.SubCategory}", 
                string.Format(AppResources.ConfirmDeleteSubCategory, 
                categoryDetail.Nombre), AppResources.Yes, AppResources.No);
            if (!answer) return;
            await Factory.DataStores.OfType<IDataStore<CategoryDetail>>().First().DeleteItemAsync(idSubCategory);
            Items.Remove(Items.Where(x=> x.CategoryDetailId == idSubCategory).FirstOrDefault());
        }


    }
}

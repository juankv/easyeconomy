﻿using System;
using System.Collections.ObjectModel;
using EasyEconomy.Models;
using EasyEconomy.Resx;
using EasyEconomy.Services.Storage;
using Xamarin.Forms;

namespace EasyEconomy.ViewModels
{
    public class NewCategoryViewModel : BaseViewModel
    {
        public IDataStore<CategoryHead> CategoryHeadStore => DependencyService.Get<IDataStore<CategoryHead>>();

        private string nombre;
        private string descripcion;
        

        public NewCategoryViewModel()
        {
            Title = $"{AppResources.Add} {AppResources.Category}";
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }

        public string Nombre
        {
            get => nombre;
            set => SetProperty(ref nombre, value);
        }

        public string Descripcion
        {
            get => descripcion;
            set => SetProperty(ref descripcion, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            CategoryHead newItem = new CategoryHead()
            {
                Nombre = nombre,
                Descripcion = descripcion,
                CreateDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                State = State.Active
            };

            await CategoryHeadStore.AddItemAsync(newItem);

            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }


        private bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(nombre)
                && !String.IsNullOrWhiteSpace(descripcion);
        }
    }
}


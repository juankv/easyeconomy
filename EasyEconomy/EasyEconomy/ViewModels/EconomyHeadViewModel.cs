﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Linq;
using EasyEconomy.Models;
using EasyEconomy.Views;
using System.Windows.Input;
using EasyEconomy.Services.Storage;
using EasyEconomy.Services.Factories;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    public class EconomyHeadViewModel : BaseViewModel
    {
        private EconomyHeadView _selectedItem;
        public ObservableCollection<EconomyHeadView> Items { get; }
        
        public Command AddEconomyHeadCommand { get; }
      //  public Command<EconomyHeadView> EconomyHeadTapped { get; }

        public EconomyHeadViewModel()
        {
            
            Items = new ObservableCollection<EconomyHeadView>();
           // EconomyHeadTapped = new Command<EconomyHeadView>(OnEconomyHeadSelected);
            AddEconomyHeadCommand = new Command(OnAddEconomyHead);
            Factory = new EconomyHeadDataStores();
        }

        async Task ExecuteLoadEconomyHeadCommand()
        {
            IsBusy = true;
            try
            {
                Items.Clear();
                var items = await Factory.DataStores.
                    OfType<IDataStore<EconomyHead>>().First().
                    GetItemsByStateAsync(State.Active);
                foreach (var item in items)
                {
                    Items.Add(new EconomyHeadView(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            SelectedItem = null;
            ExecuteLoadEconomyHeadCommand();
            Title = AppResources.TitleEconomiesPage;
            IsBusy = false;
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetProperty(ref _isRefreshing, value); }
        }


        public EconomyHeadView SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
            }
        }

        public ICommand ItemTappedCommand => new Command(async (item) => await OnItemTappedCommandExecuted(item));

        private async Task OnItemTappedCommandExecuted(object item)
        {
            if (item == null)
                return;
            var order = item as Xamarin.Forms.ItemTappedEventArgs;
            var itemSelect = order.Item as EconomyHeadView;
            App.EconomyId =int.Parse(itemSelect.Id);
            await Shell.Current.GoToAsync($"{nameof(EconomyDetailPage)}?{nameof(EconomyDetailViewModel.EconomyHeadId)}={itemSelect.Id}");
        }


       

        private async void OnAddEconomyHead(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewEconomyYearPage));
        }

        public async Task ExecuteEditEconomyCommand(string idEconomy)
        {
            await Shell.Current.GoToAsync($"{nameof(UpdateEconomyPage)}?{nameof(UpdateEconomyViewModel.EconomyHeadId)}={idEconomy}");
        }

        public async Task ExecuteDeleteEconomyCommand(string idEconomy)
        {
            
            var economy = await Factory.DataStores.OfType<IDataStore<EconomyHead>>().First().GetItemAsync(idEconomy);
            var movements = await Factory.DataStores.
                OfType<IDetailDataStore<EconomyDetail, EconomyHead>>()
                .First().GetItemsAsync(economy, State.Active);

            var answer = await Application.Current.MainPage.DisplayAlert(
                $"{AppResources.Delete} {AppResources.Economy}", 
                string.Format( AppResources.ConfirmDeleteEconomy, 
                economy.Nombre, movements.Count().ToString()), AppResources.Yes, AppResources.No);

            if (!answer) return;

            foreach (var item in movements)
            {
                await Factory.DataStores.OfType<IDataStore<EconomyDetail>>().First().DeleteItemAsync(item.EconomyDetailId.ToString());
            }

            await Factory.DataStores.OfType<IDataStore<EconomyHead>>().First().DeleteItemAsync(idEconomy);
            Items.Remove(Items.Where(x => x.Id == idEconomy).FirstOrDefault());
        }

    }

}

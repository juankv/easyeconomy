﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using EasyEconomy.Models;
using EasyEconomy.Resx;
using Xamarin.Forms;

namespace EasyEconomy.ViewModels
{
    [QueryProperty(nameof(EconomyHeadId), nameof(EconomyHeadId))]
    public class UpdateEconomyViewModel: EconomyViewModel
    {
        private string economyHeadId;
        public string EconomyHeadId
        {
            get
            {
                return economyHeadId;
            }
            set
            {
                economyHeadId = value;
            }
        }
        public async void OnAppearing()
        {
            var itemupdate = await EconomyHeadStore.GetItemAsync(economyHeadId);
            Nombre = itemupdate.Nombre;
            Presupuesto = itemupdate.Presupuesto;
            IsBusy = false;
        }



        public UpdateEconomyViewModel() : base()
        {
            Title = $"{AppResources.Update} {AppResources.Economy}";
            UpdateCommand = new Command(OnUpdate, ValidateSave);
            this.PropertyChanged +=
                (_, __) => UpdateCommand.ChangeCanExecute();
        }

        public Command UpdateCommand { get; }
        private async void OnUpdate()
        {
            var id = EconomyHeadId;
            UpdateItemHead(id);
            await Shell.Current.GoToAsync("..");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using EasyEconomy.Models;
using EasyEconomy.Resx;
using EasyEconomy.Services.Storage;
using EasyEconomy.Views;
using Xamarin.Forms;

namespace EasyEconomy.ViewModels
{
    [QueryProperty(nameof(CategoryHeadId), nameof(CategoryHeadId))]
    public class NewSubCategoryViewModel : BaseViewModel
    {
        public IDataStore<CategoryDetail> CategoryDetailStore => DependencyService.Get<IDataStore<CategoryDetail>>();
        private string nombre;
        private string descripcion;
        private string categoryHeadId;
        public NewSubCategoryViewModel()
        {
            Title = $"{AppResources.Add} {AppResources.SubCategory}";
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }

        public string Nombre
        {
            get => nombre;
            set => SetProperty(ref nombre, value);
        }

        public string Descripcion
        {
            get => descripcion;
            set => SetProperty(ref descripcion, value);
        }


        public string CategoryHeadId
        {
            get => categoryHeadId;
            set => SetProperty(ref categoryHeadId, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");              

        }

        private async void OnSave()
        {
            var intCategoryHeadId = int.Parse(CategoryHeadId);
            CategoryDetail newItem = new CategoryDetail()
            {
                CategoryHeadId = intCategoryHeadId,
                Nombre = Nombre,
                Descripcion = Descripcion,
                ModifiedDate = DateTime.Now,
                CreateDate = DateTime.Now,
                State = State.Active
            };

            await CategoryDetailStore.AddItemAsync(newItem);
            Shell.Current.IsBusy = false;
            await Shell.Current.GoToAsync($"..?{nameof(SubCategoryViewModel.CategoryHeadId)}={CategoryHeadId}");
        }


        private bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(nombre)
                && !String.IsNullOrWhiteSpace(descripcion);
        }
    }
}

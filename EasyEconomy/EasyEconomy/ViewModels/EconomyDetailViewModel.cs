﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using EasyEconomy.Models;
using EasyEconomy.Views;
using SQLite;
using Xamarin.Forms;
using System.Linq;
using EasyEconomy.Services.Storage;
using EasyEconomy.Services.Factories;
using EasyEconomy.Resx;

namespace EasyEconomy.ViewModels
{
    [QueryProperty(nameof(EconomyHeadId), nameof(EconomyHeadId))]
    public class EconomyDetailViewModel : BaseViewModel
    {
        private string economyHeadId;
        private string nombre;
        private string presupuesto;
        private string fechaCreacion;
        private string fechaCorte;
        private string totalSalidas;
        private string diferenciaSalidaEntrada;
        private string diferenciaPresupuesto;
        public string Id { get; set; }

        private EconomyDetailView _selectedItem;

        public EconomyDetailView SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
            }
        }

        private string totalEntradas;

        public string TotalEntradas
        {
            get => totalEntradas;
            set => SetProperty(ref totalEntradas, value);
        }

        public string TotalSalidas
        {
            get => totalSalidas;
            set => SetProperty(ref totalSalidas, value);
        }

        public string DiferenciaPresupuesto
        {
            get => diferenciaPresupuesto;
            set => SetProperty(ref diferenciaPresupuesto, value);
        }

        public string DiferenciaEntradaSalida
        {
            get => diferenciaSalidaEntrada;
            set => SetProperty(ref diferenciaSalidaEntrada, value);
        }


        public ObservableCollection<EconomyDetailView> Items { get; }

        public string Nombre
        {
            get => nombre;
            set => SetProperty(ref nombre, value);
        }

        public string Presupuesto
        {
            get => presupuesto;
            set => SetProperty(ref presupuesto, value);
        }

        public string FechaCorte
        {
            get => fechaCorte;
            set => SetProperty(ref fechaCorte, value);
        }

        public string FechaCreacion
        {
            get => fechaCreacion;
            set => SetProperty(ref fechaCreacion, value);
        }

        public string EconomyHeadId
        {
            get
            {
                return economyHeadId;
            }
            set
            {
                economyHeadId = value;
                LoadItemId(value);
            }
        }
        public Command AddEconomyDetailCommand { get; }

        public EconomyDetailViewModel()
        {
            Items = new ObservableCollection<EconomyDetailView>();
            AddEconomyDetailCommand = new Command(OnAddEconomyDetail);
            Factory = new EconomyDetailDataStores();
        }

        public void OnAppearing()
        {
            SelectedItem = null;
            IsBusy = false;

            
        }


        public async Task ExecuteEditEconomyDeatilCommand(string idEconomy, string IdEconomyDetail)
        {
            await Shell.Current.GoToAsync($"{nameof(UpdateEconomyDetailPage)}?{nameof(UpdateEconomyDetailViewModel.EconomyHeadId)}={EconomyHeadId}&{nameof(UpdateEconomyDetailViewModel.EconomyDetailId)}={IdEconomyDetail}");
        }

        public async Task ExecuteDeleteEconomyDeatilCommand(EconomyDetailView view)
        {
            var movement = await Factory.DataStores.OfType<IDataStore<EconomyDetail>>().First().GetItemAsync(view.IdEconomyDetail);
            var answer = await Application.Current.MainPage.DisplayAlert($"{AppResources.Delete} {AppResources.Movement}", 
                string.Format(AppResources.ConfirmDeleteMovement, movement.Amount.ToString("C")) , AppResources.Yes, AppResources.No);
            if (!answer) return;
            await Factory.DataStores.OfType<IDataStore<EconomyDetail>>().First().DeleteItemAsync(view.IdEconomyDetail);
            Items.Remove(Items.Where(x=> x.IdEconomyDetail == view.IdEconomyDetail).FirstOrDefault());           
        }



        private async void OnAddEconomyDetail(object obj)
        {
            App.DateCalendar = DateTime.Now;
            await Shell.Current.GoToAsync($"{nameof(NewEconomyDetailPage)}?{nameof(NewEconomyDetailViewModel.EconomyHeadId)}={EconomyHeadId}");
        }

        double totalEntradasInt;
        double totalSalidasInt;
        double presupuestoInt;
        double diferenciaEntradaSalidaInt ;
        double diferenciaPresupuestoInt ;

        public async void BiuldHeadInformation(EconomyHead item)
        {
            Id = item.EconomyHeadId.ToString();
            Nombre = char.ToUpper(item.Nombre[0]) + item.Nombre.Substring(1);
            Presupuesto = item.Presupuesto.ToString("C0");
            FechaCreacion = item.FechaCreacion.ToString("yyyy-MM-dd HH:mm:ss");
            FechaCorte = item.FechaCorte.ToString("yyyy-MM-dd HH:mm:ss");
            totalEntradasInt = await BusinessLogicQuery.TotalIn(item.EconomyHeadId.ToString());
            totalSalidasInt = await BusinessLogicQuery.TotalOut(item.EconomyHeadId.ToString());
            presupuestoInt = item.Presupuesto;
            diferenciaEntradaSalidaInt = totalEntradasInt + totalSalidasInt;
            diferenciaPresupuestoInt = presupuestoInt + diferenciaEntradaSalidaInt;
            TotalEntradas = totalEntradasInt.ToString("C0");
            TotalSalidas = totalSalidasInt.ToString("C0");
            DiferenciaEntradaSalida = Math.Abs(diferenciaEntradaSalidaInt).ToString("C0");
            DiferenciaPresupuesto = diferenciaPresupuestoInt.ToString("C0");
        }

        public async void BuildList(EconomyHead item)
        {
            var economyDetail = (await Factory.DataStores.
                OfType<IDetailDataStore<EconomyDetail, EconomyHead>>().
                First().GetItemsAsync(item, State.Active)).
                OrderByDescending(x => x.CreateDate);

            foreach (var itemDetail in economyDetail)
            {
                
                if (itemDetail.CategoryDetailId == 0)
                {
                    Items.Add(new EconomyDetailView(itemDetail));
                }
                else 
                {
                    var cd = await Factory.DataStores.OfType<IDataStore<CategoryDetail>>().First().GetItemAsync(itemDetail.CategoryDetailId.ToString());
                    var ch = await Factory.DataStores.OfType<IDataStore< CategoryHead>>().First().GetItemAsync(cd.CategoryHeadId.ToString());
                    Items.Add(new EconomyDetailView(itemDetail, cd, ch));
                }
            }
        }

        public async void LoadItemId(string itemId)
        {
            if (IsBusy)
                return;
            try
            {
                Items.Clear();
                IsBusy = true;
                if (string.IsNullOrEmpty(itemId))
                    itemId = App.EconomyId.ToString();

                var economyHead = await Factory.DataStores.OfType<IDataStore<EconomyHead>>().First().GetItemAsync(itemId);
                Title = economyHead.Nombre +"-"+ AppResources.TitleEconomyDetailPage;
                BiuldHeadInformation(economyHead);
                BuildList(economyHead);
                App.EconomyId =int.Parse( itemId);
               
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Failed to Load Item");
            }
            finally {
                await Task.Delay(200);
                IsBusy = false;
            }
        }
    }
}
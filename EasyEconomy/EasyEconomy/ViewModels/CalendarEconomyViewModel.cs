﻿using EasyEconomy.Models;
using EasyEconomy.Services.Factories;
using EasyEconomy.Services.Storage;
using EasyEconomy.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using Xamarin.Plugin.Calendar.Models;


namespace EasyEconomy.ViewModels
{
    public class CalendarEconomyViewModel: BaseViewModel
    {
        private DateTime _monthYear = DateTime.Today;

        public DateTime MonthYear
        {
            get => _monthYear;
            set => SetProperty(ref _monthYear, value);
        }
        public ICommand DayTappedCommand => new Command<DateTime>(async (date) => await DayTapped(date));
        public ICommand SwipeLeftCommand => new Command(() => { MonthYear = MonthYear.AddMonths(2); GetEconomyEvents(); });
        public ICommand SwipeRightCommand => new Command(() => { MonthYear = MonthYear.AddMonths(-2); GetEconomyEvents(); });
        public ICommand SwipeUpCommand => new Command(() => { MonthYear = DateTime.Today; });
        private string selectedDate;

        private async Task DayTapped(DateTime date)
        {
            var message = $"Received tap event from date: {date}";
            App.DateCalendar = date;
            //GetEconomyEvents();
            await Task.CompletedTask;
            //await App.Current.MainPage.DisplayAlert("DayTapped", message, "Ok");
        }

        private async void OnAddEconomyDetail(object obj)
        {
            var id = App.EconomyId;
            await Shell.Current.GoToAsync($"{nameof(NewEconomyDetailPage)}?{nameof(NewEconomyDetailViewModel.EconomyHeadId)}={id}");

        }
        public string SelectedDate
        {
            get { return selectedDate; }
            set
            {
                if (selectedDate != value)
                {
                    selectedDate = value;
                    App.DateCalendar = DateTime.Parse(value);
                    OnPropertyChanged();
                }
            }
        }
        public EventCollection Events { get; set; }
        public Command AddEconomyDetailCommand { get; }

        public CalendarEconomyViewModel()
        {
            Title = "Calendar Economies";
            Events = new EventCollection();
            AddEconomyDetailCommand = new Command(OnAddEconomyDetail);
            Factory = new CalendarDataStores();
            
        }

            //hay que pensar en la forma de agrupar los que tienen el mismo dia pero diferente hora. porque parece que el 
            // componente se estalla cuando se le pasa dos fechas con el mismo dia he iguales 
        public async void GetEconomyEvents()
        {
            var FirstDay = new DateTime(App.DateCalendar.Year, App.DateCalendar.Month, 1);
            var month = FirstDay.Month;
            Events.Clear();

            var collection = new List<EconomyDetailView>();
            var economyDetailsAtMonth = await BusinessLogicQuery.DetailBetweenDatesSortByDate(FirstDay, FirstDay.AddMonths(1));

            var day2 = economyDetailsAtMonth.Count > 0? economyDetailsAtMonth[0].CreateDate.Day: 1;
            var count = economyDetailsAtMonth.Count;
            var count1 = 0;
            foreach (var economyDetail in economyDetailsAtMonth)
            {
                count1++;
                var day3 = economyDetail.CreateDate.Day;
                if (day2 != day3)
                {
                    day2 = day3;
                    Events.Add(FirstDay, collection);
                    collection = new List<EconomyDetailView>();
                }
                FirstDay = new DateTime(economyDetail.CreateDate.Year, economyDetail.CreateDate.Month, economyDetail.CreateDate.Day);

                var cd = await Factory.DataStores.OfType<IDataStore<CategoryDetail>>().First().GetItemAsync(economyDetail.CategoryDetailId.ToString());
                var ch = await Factory.DataStores.OfType<IDataStore<CategoryHead>>().First().GetItemAsync(cd.CategoryHeadId.ToString());
                collection.Add(new EconomyDetailView(economyDetail, cd, ch));
                

                if (count1 == count && collection.Count > 0)
                    Events.Add(FirstDay, collection);
            }
            /*
            while (month == FirstDay.Month) {
                var economyDetail2 = await BusinessLogicQuery.DetailBetweenDatesSortByDate(FirstDay, FirstDay.AddDays(1));
                
                if(economyDetail2.Count != 0) {
                    foreach (var item in economyDetail2)
                    {
                        var detailmap = await EconomyDetailMapStore1.GetItemsAsync(item);
                        if (detailmap.Count > 0)
                        {
                            var cd = await CategoryDetailStore.GetItemAsync(detailmap[0].CategoryDetailId.ToString());
                            var ch = await CategoryHeadStore.GetItemAsync(cd.CategoryHeadId.ToString());
                            collection.Add(new EconomyDetailView(item, cd, ch));
                        }
                        else
                        {
                            collection.Add(new EconomyDetailView(item, null, null));
                        }
                    }
                    Events.Add(FirstDay, collection);
                }
                FirstDay = FirstDay.AddDays(1);
            }*/



        }

        public void OnAppearing()
        {
                
            GetEconomyEvents();
            IsBusy = false;
        }
    }

}

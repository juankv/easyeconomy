﻿using Xamarin.Forms;

namespace EasyEconomy.Effects
{
    public class RemoveEntryUnderline: RoutingEffect
    {
        public RemoveEntryUnderline()
           : base("EasyEconomy.RemoveEntryUnderline")
        {
        }
    }
}

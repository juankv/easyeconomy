﻿using Xamarin.Forms;

namespace EasyEconomy.Effects
{
    public class RemoveEntryBordersEffect : RoutingEffect
    {
        public RemoveEntryBordersEffect()
            : base("EasyEconomy.RemoveEntryBordersEffect")
        {
        }
    }
}

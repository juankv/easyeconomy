﻿using EasyEconomy.Models;
using EasyEconomy.Services.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EasyEconomy.Storage
{
    public class BusinessLogicQuery : BaseDatabase, IBusinessLogicQuery
    {
       

        public async Task<List<EconomyDetail>> DetailBetweenDatesSortByDate(DateTime start, DateTime end)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.Table<EconomyDetail>().Where(x => (
            x.CreateDate >= start && x.CreateDate < end)).ToListAsync();
        }

        

        public async Task<List<EconomyDetail>> DetailByHeadIdsSortByDate(string ids)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.QueryAsync<EconomyDetail>
                ($"select * from EconomyDetail where EconomyHeadId in ({ids}) order by Fecha");

        }

        public async Task<double> TotalIn(string id)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return databaseConnection.ExecuteScalarAsync<int>($"SELECT SUM(Amount) FROM  EconomyDetail where EconomyHeadId = {id} and Kind in ('Entada', '+')").Result;
        }

        public async Task<double> TotalOut(string id)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return databaseConnection.ExecuteScalarAsync<int>($"SELECT SUM(Amount) FROM  EconomyDetail where EconomyHeadId = {id} and Kind in ('Salida', '-')").Result;

        }

        public async void EliminarDetalleMap(string idDetail)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            await databaseConnection.ExecuteAsync("DELETE FROM EconomyDetailMap Where CategoryDetailId = ?",   int.Parse(idDetail));
        }

        public async void QuitarSubCategoryDetalleMap(string idDetail)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            await databaseConnection.ExecuteAsync("UPDATE EconomyDetailMap SET CategoryDetailId = null Where CategoryDetailId = ?", int.Parse(idDetail));
        }


        public async void EliminarDetalleMap()
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            await databaseConnection.ExecuteAsync("DELETE FROM EconomyDetailMap");
        }

        public async Task<double> TotalOut()
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return databaseConnection.ExecuteScalarAsync<int>($"SELECT SUM(Amount) FROM  EconomyDetail where Kind in ('Salida', '-')").Result;
        }

        public async Task<double> TotalIn()
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false); 
            return databaseConnection.ExecuteScalarAsync<int>($"SELECT SUM(Amount) FROM  EconomyDetail where Kind in ('Entada', '+')").Result;
        }

    }
}

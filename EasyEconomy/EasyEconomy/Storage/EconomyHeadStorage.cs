﻿using EasyEconomy.Models;
using EasyEconomy.Services.Storage;
using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace EasyEconomy.Storage
{
    public class EconomyHeadStorage : BaseDatabase, IDataStore<EconomyHead>
    {
        public EconomyHeadStorage()
        {
            CreateTable();
        }
        public async Task<int> AddItemAsync(EconomyHead item)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.InsertAsync(item).ConfigureAwait(false);
        }

        public async void CreateTable()
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            await conection.CreateTableAsync<EconomyHead>(CreateFlags.AutoIncPK);
        }

        public async Task<int> DeleteItemAsync(string id)
        {
            var oldItem = await BuildDelete(id);
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);            
            return await conection.DeleteAsync(oldItem).ConfigureAwait(false);
        }

        private async Task<EconomyHead> BuildDelete(string id)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var idint = int.Parse(id);
            var oldItem = await conection.Table<EconomyHead>().FirstOrDefaultAsync((EconomyHead arg) => arg.EconomyHeadId == idint);
            oldItem.ModifiedDate = System.DateTime.Now;
            oldItem.State = State.Inactive;
            return oldItem;
        }

        public async Task<EconomyHead> GetItemAsync(string id)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var idint = int.Parse(id);
            return await conection.Table<EconomyHead>().FirstOrDefaultAsync((EconomyHead arg) => arg.EconomyHeadId == idint);
        }

        public async Task<IEnumerable<EconomyHead>> GetItemsAsync(bool forceRefresh = false)
        { 
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await conection.Table<EconomyHead>().Where(x=> x.EconomyHeadId > 0).ToListAsync();
        }

        public async Task<int> UpdateItemAsync(EconomyHead item)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.UpdateAsync(item).ConfigureAwait(false);
        }

        public async Task<IEnumerable<EconomyHead>> GetItemsByStateAsync(State state)
        {
            var items = await GetItemsAsync();
            return items.Where(x => x.State == state);
        }
    }
}

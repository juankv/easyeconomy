﻿using EasyEconomy.Models;
using EasyEconomy.Services.Storage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace EasyEconomy.Storage
{
    public class EconomyDetailStorage : BaseDatabase, 
        IDataStore<EconomyDetail>,
        IDetailDataStore<EconomyDetail, EconomyHead>
    {
        public EconomyDetailStorage()
        {
            CreateTable();
        }
        public async Task<int> AddItemAsync(EconomyDetail item)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.InsertAsync(item).ConfigureAwait(false);
        }

        public async void CreateTable()
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            await conection.CreateTableAsync<EconomyDetail>(CreateFlags.AutoIncPK);
        }

        public async Task<int> DeleteItemAsync(string id)
        {
            var oldItem = await BuildDelete(id);
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await conection.UpdateAsync(oldItem).ConfigureAwait(false);
        }

        private async Task<EconomyDetail> BuildDelete(string id)
        {
            var idint = int.Parse(id);
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var oldItem = await conection.Table<EconomyDetail>().FirstOrDefaultAsync((EconomyDetail arg) => arg.EconomyDetailId == idint);
            oldItem.State = State.Inactive;
            oldItem.ModifiedDate = DateTime.Now;
            return oldItem;
        }

        public async Task<EconomyDetail> GetItemAsync(string id)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var intId = int.Parse(id);
            return await conection.Table<EconomyDetail>().FirstOrDefaultAsync((EconomyDetail arg) => arg.EconomyDetailId == intId);
        }

        public async Task<IEnumerable<EconomyDetail>> GetItemsAsync(bool forceRefresh = false)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await conection.Table<EconomyDetail>().Where(x=> x.EconomyDetailId > 0).ToListAsync();
        }

        public async Task<List<EconomyDetail>> GetItemsAsync(EconomyHead ItemSearch, State state)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var id =  ItemSearch.EconomyHeadId;
            return await conection.Table<EconomyDetail>().Where((EconomyDetail arg) => arg.EconomyHeadId == id && arg.State == state).ToListAsync();

        }

        public async Task<int> UpdateItemAsync(EconomyDetail item)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.UpdateAsync(item).ConfigureAwait(false);
        }

        public async Task<IEnumerable<EconomyDetail>> GetItemsByStateAsync(State state)
        {
            var items = await GetItemsAsync();
            return items.Where(x => x.State == state);
        }
    }
}

﻿using EasyEconomy.Services.Storage;
using SQLite;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EasyEconomy.Storage
{
    public abstract class BaseDatabase
    {
        #region Constant Fields
        static Lazy<SQLiteAsyncConnection> _databaseConnectionHolder = new Lazy<SQLiteAsyncConnection>(() =>
            DependencyService.Get<ISQLite>().GetConnection());
        #endregion
        protected static async Task<SQLiteAsyncConnection> GetDatabaseConnectionAsync()
        {
            return await Task.FromResult(_databaseConnectionHolder.Value);
        }

      
    }
}

﻿using EasyEconomy.Models;
using EasyEconomy.Services.Storage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace EasyEconomy.Storage
{
    public class CategoryDetailStorage : BaseDatabase, IDataStore<CategoryDetail>, IDetailDataStore<CategoryDetail, CategoryHead>
    {
        public CategoryDetailStorage()
        {
            CreateTable();
        }

        public async Task<int> AddItemAsync(CategoryDetail item)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.InsertAsync(item).ConfigureAwait(false);
            
        }

        public async void CreateTable()
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            await conection.CreateTableAsync<CategoryDetail>(CreateFlags.AutoIncPK);
        }

        public async Task<int> DeleteItemAsync(string id)
        {
            var oldItem = await BuildDelete(id);
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await conection.UpdateAsync(oldItem).ConfigureAwait(false);
        }

        private async Task<CategoryDetail> BuildDelete(string id)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var idint = int.Parse(id);
            var oldItem = await conection.Table<CategoryDetail>().FirstOrDefaultAsync((CategoryDetail arg) => arg.CategoryDetailId == idint);
            oldItem.State = State.Inactive;
            oldItem.ModifiedDate = DateTime.Now;
            return oldItem;
        }

        public async Task<CategoryDetail> GetItemAsync(string id)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var intId = int.Parse(id);
            return await conection.Table<CategoryDetail>().FirstOrDefaultAsync((CategoryDetail arg) => arg.CategoryDetailId == intId);
        }

        public async Task<IEnumerable<CategoryDetail>> GetItemsAsync(bool forceRefresh = false)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await conection.Table<CategoryDetail>().ToListAsync();
        }

        public async Task<List<CategoryDetail>> GetItemsAsync(CategoryHead ItemSearch, State state)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var id = ItemSearch.CategoryHeadId;
            return await conection.Table<CategoryDetail>().Where((CategoryDetail arg) => arg.CategoryHeadId == id && arg.State == state).ToListAsync();
        }

        public async Task<int> UpdateItemAsync(CategoryDetail item)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.UpdateAsync(item).ConfigureAwait(false);
        }

        public async Task<IEnumerable<CategoryDetail>> GetItemsByStateAsync(State state)
        {
            var items = await GetItemsAsync();
            return items.Where(x => x.State == state);
        }
    }
}

﻿using EasyEconomy.Models;
using EasyEconomy.Services.Storage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace EasyEconomy.Storage
{
    public class CategoryHeadStorage : BaseDatabase, IDataStore<CategoryHead>
    {
        public CategoryHeadStorage()
        {
            CreateTable();
        }
        public async Task<int> AddItemAsync(CategoryHead item)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.InsertAsync(item).ConfigureAwait(false);
        }

        public async void CreateTable()
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            await conection.CreateTableAsync<CategoryHead>(CreateFlags.AutoIncPK);
        }

        public async Task<int> DeleteItemAsync(string id)
        {
            var oldItem = await BuildDelete(id);
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);            
            return await conection.UpdateAsync(oldItem).ConfigureAwait(false);
        }

        private async Task<CategoryHead> BuildDelete(string id)
        {
            var idint = int.Parse(id);
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var oldItem = await conection.Table<CategoryHead>().FirstOrDefaultAsync((CategoryHead arg) => arg.CategoryHeadId == idint);
            oldItem.State = State.Inactive;
            oldItem.ModifiedDate = DateTime.Now;
            return oldItem;
        }

        public async Task<CategoryHead> GetItemAsync(string id)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            var intId = int.Parse(id);
            return await conection.Table<CategoryHead>().FirstOrDefaultAsync((CategoryHead arg) => arg.CategoryHeadId == intId);
        }

        public async Task<IEnumerable<CategoryHead>> GetItemsAsync(bool forceRefresh = false)
        {
            var conection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await conection.Table<CategoryHead>().ToListAsync();
        }

        public async Task<int> UpdateItemAsync(CategoryHead item)
        {
            var databaseConnection = await GetDatabaseConnectionAsync().ConfigureAwait(false);
            return await databaseConnection.UpdateAsync(item).ConfigureAwait(false);
        }

        public async Task<IEnumerable<CategoryHead>> GetItemsByStateAsync(State state)
        {
            var items = await GetItemsAsync();
            return items.Where(x => x.State == state);
        }
    }
}

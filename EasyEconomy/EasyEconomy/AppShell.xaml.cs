﻿using System;
using System.Collections.Generic;
using EasyEconomy.Services.Auth;
using EasyEconomy.ViewModels;
using EasyEconomy.Views;
using Xamarin.Forms;

namespace EasyEconomy
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(NewEconomyYearPage), typeof(NewEconomyYearPage));
            Routing.RegisterRoute(nameof(EconomyDetailPage), typeof(EconomyDetailPage));
            Routing.RegisterRoute(nameof(NewCategoryPage), typeof(NewCategoryPage));
            Routing.RegisterRoute(nameof(NewEconomyDetailPage), typeof(NewEconomyDetailPage));
            Routing.RegisterRoute(nameof(SubCategoryPage), typeof(SubCategoryPage));
            Routing.RegisterRoute(nameof(NewSubCategoryPage), typeof(NewSubCategoryPage));
            Routing.RegisterRoute(nameof(UpdateEconomyDetailPage), typeof(UpdateEconomyDetailPage));
            Routing.RegisterRoute(nameof(UpdateEconomyPage), typeof(UpdateEconomyPage));
            Routing.RegisterRoute(nameof(UpdateCategoryPage), typeof(UpdateCategoryPage));
            Routing.RegisterRoute(nameof(UpdateSubCategoryPage), typeof(UpdateSubCategoryPage));
            //Shell.Current.GoToAsync("//LoginPage");
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//LoginPage");
        }
    }
}

﻿using System;
using SQLite;


namespace EasyEconomy.Models
{
    public class AppAction
    {
        [PrimaryKey, AutoIncrement]
        public int AppActionId { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string TypeValue { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEconomy.Models
{
    public class EconomyHeadView
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Presupuesto { get; set; }
        public string Excedente { get; set; }
        public string Deficid { get; set; }
        public string MontoFinal { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaCorte { get; set; }

        public EconomyHeadView(EconomyHead eh)
        {
            this.Id = eh.EconomyHeadId.ToString();
            this.Nombre = char.ToUpper(eh.Nombre[0]) + eh.Nombre.Substring(1);
            this.Deficid = eh.Deficid.ToString("C0");
            this.MontoFinal = eh.MontoFinal.ToString("C0");
            this.Excedente = eh.Excedente.ToString("C0");
            this.Presupuesto = eh.Presupuesto.ToString("C0");
            this.FechaCreacion = eh.FechaCreacion.ToString("yyyy-MM-dd HH:mm:ss");
            this.FechaCorte = eh.FechaCorte.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEconomy.Models
{
    public class ChartSubCategoryInfo : ChartCategoryInfo
    {
        public int Id2 { get; set; }

    }

    public class ChartCategoryInfo
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public string Name { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEconomy.Models
{
    public class CategoryHead
    {
        [PrimaryKey, AutoIncrement]
        public int CategoryHeadId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public State State { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}

﻿using SQLite;
using System;

namespace EasyEconomy.Models
{
    public enum ViewControl
    {
        editor,
        entry,
        picker,
        button,
        item
    }

    public enum Action
    {
        ItemTapped,
        Refreshing,
        ButtonPress
    }

    public class ViewAction
    {
        [PrimaryKey, AutoIncrement]
        public int ViewActionId { get; set; }

        public ViewControl ViewControl { get; set; }

        public Action Action { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}

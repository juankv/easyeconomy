﻿using SQLite;
using System;

namespace EasyEconomy.Models
{
    public enum Period
    {
        PerMonth,
        ByYear,
        PerDay,
        PerWeek
    }

    public class EconomyHead
    {
        public string Nombre { get; set; }
        public double Presupuesto { get; set; }
        public double Excedente { get; set; }
        public double Deficid { get; set; }
        public double MontoFinal { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaCorte { get; set; }
        public DateTime ModifiedDate { get; set; }

        public Period PeriodOfTime { get; set; }
        public State State { get; set; }

        [PrimaryKey, AutoIncrement]
        public int EconomyHeadId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEconomy.Models
{
    public class PickerValue
    {
        public string Id { get; set; }
        public string Value { get; set; }

        public PickerValue(string id, string value)
        {
            this.Id = id;
            this.Value = value;
        }
    }
}

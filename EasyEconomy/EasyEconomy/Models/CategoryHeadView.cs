﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEconomy.Models
{
    public class CategoryHeadView
    {
        public string CategoryHeadId { get; set; }
        public string Nombre { get; set; }

        public CategoryHeadView(CategoryHead ch)
        {
            this.CategoryHeadId = ch.CategoryHeadId.ToString();
            this.Nombre = char.ToUpper(ch.Nombre[0]) + ch.Nombre.Substring(1);
        }
    }
}

﻿using SQLite;
using System;

namespace EasyEconomy.Models
{
    public enum State
    {
        PaidOut, //Aplicado a los movimientos   
        ToPay, //Aplicado a los movimientos
        Active, //Cuando una fila fue ingresada en el periodo actual
        Inactive, //Cuando una fila fue eliminada mientras su periodo de creacion estaba abierto
        Archived //Es cuando un dato de economia o de movimiento a finalizado el periodo 
    }

    public enum QuantityUnit
    {
        ct, //Cantidad contada de una
        grms, // gramos
        lb, // libras
        kg, // kilogramos
        sl, // (sl), una «unidad imperial» de masa (unos 14,6 kg)
        oz //onzas
    }

    public class EconomyDetail
    {
        
        [PrimaryKey, AutoIncrement]
        public int EconomyDetailId { get; set; }
        [Indexed]
        public int EconomyHeadId { get; set; }
        public double Amount { get; set; }
        public double HopeAmount { get; set; }
        public string Kind { get; set; }
        public DateTime PayDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int CategoryDetailId { get; set; }
        public int Quantity { get; set; }
        public QuantityUnit QuantityUnit { get; set; }
        public State State { get; set; }

        public EconomyDetail()
        {

        }
    }
}

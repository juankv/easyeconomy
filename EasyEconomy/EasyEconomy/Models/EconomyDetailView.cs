﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EasyEconomy.Models
{
    public class EconomyDetailView
    {
        public string IdEconomyDetail { get; set; }
        public string IdEconomyHead { get; set; }
        public string IdCategory { get; set; }
        public string IdSubcategory { get; set; }
        public string Amount { get; set; }
        public string Tipo { get; set; }
        public string Fecha { get; set; }
        public string CategoryHead { get; set; }
        public string CategoryDetail { get; set; }
        public bool Selected { get; set; }

        public Color BackgroundColor => Selected ? Color.Green : Color.Red;


        public string Category { get; set; }

        public EconomyDetailView(EconomyDetail ed)
        {
            FillEconomyDetailInfo(ed);
        }

        public void FillEconomyDetailInfo(EconomyDetail ed)
        {
            this.IdEconomyDetail = ed.EconomyDetailId.ToString();
            this.IdEconomyHead = ed.EconomyHeadId.ToString();
            this.Amount = ed.Amount.ToString("C0");
            this.Tipo = ed.Kind;
            this.Selected = this.Tipo == "+" ? true : false;
            this.Fecha = ed.ModifiedDate.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public void FillCategoryDetailInfo(CategoryHead ch)
        {
            this.CategoryHead = char.ToUpper(ch.Nombre[0]) + ch.Nombre.Substring(1);
            this.Category = char.ToUpper(ch.Nombre[0]) + ch.Nombre.Substring(1);
            this.IdCategory = ch.CategoryHeadId.ToString();
        }

        public void FillSubCategory(CategoryDetail cd)
        {
            this.CategoryDetail = char.ToUpper(cd.Nombre[0]) + cd.Nombre.Substring(1);
            this.Category += " - " + char.ToUpper(cd.Nombre[0]) + cd.Nombre.Substring(1);
            this.IdSubcategory = cd.CategoryDetailId.ToString();
        }

        public EconomyDetailView(EconomyDetail ed, CategoryDetail cd, CategoryHead ch)
        {           
            FillEconomyDetailInfo(ed);
            FillCategoryDetailInfo(ch);
            FillSubCategory(cd);
        }

    }
}

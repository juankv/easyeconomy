﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EasyEconomy.Models
{
    public class SubCategoryView
    {
        public string CategoryDetailId { get; set; }
        public string CategoryHeadId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }


        public SubCategoryView(CategoryDetail cd)
        {
            this.CategoryHeadId = cd.CategoryHeadId.ToString();
            this.CategoryDetailId = cd.CategoryDetailId.ToString();
            this.Nombre = char.ToUpper(cd.Nombre[0]) + cd.Nombre.Substring(1);
            this.Descripcion = char.ToUpper(cd.Descripcion[0]) + cd.Descripcion.Substring(1);
        }
    }
}

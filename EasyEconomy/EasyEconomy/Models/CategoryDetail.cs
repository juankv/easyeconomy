﻿using SQLite;
using System;

namespace EasyEconomy.Models
{
    public class CategoryDetail
    {
        [PrimaryKey, AutoIncrement]
        public int CategoryDetailId { get; set; }
        [Indexed]
        public int CategoryHeadId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public State State { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}

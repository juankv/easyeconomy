﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;

namespace EasyEconomy.Components
{
    public partial class OutlinedMaterialEditor : Grid
    {
        private ImageSource tempIcon;

        public static readonly BindableProperty TextProperty = BindableProperty.Create(
            nameof(Text),
            typeof(string),
            typeof(OutlinedMaterialEditor),
            default(string),
            BindingMode.TwoWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;
                view.customEditor.Text = (string)newValue;
                var oldValueIn = (string)oldValue;
                if (oldValueIn == null && view.customEditor.Text.Count() > 0)
                {
                    MainThread.BeginInvokeOnMainThread(async () => {
                        await view.ControlFocusedColor();
                        view.UnfocusedEditorColor();
                    });
                }
            }
        );


        public static readonly BindableProperty PlaceholderTextProperty = BindableProperty.Create(
            nameof(PlaceholderText),
            typeof(string),
            typeof(OutlinedMaterialEditor),
            default(string),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;

                view.placeholderTextEditor.Text = (string)newValue;
            }
        );

        public static readonly BindableProperty HelperTextProperty = BindableProperty.Create(
            nameof(HelperText),
            typeof(string),
            typeof(OutlinedMaterialEditor),
            default(string),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;

                view.helperTextEditor.Text = (string)newValue;

                if (view.errorTextEditor.IsVisible)
                    view.helperTextEditor.IsVisible = false;
                else
                    view.helperTextEditor.IsVisible = !string.IsNullOrEmpty(view.helperTextEditor.Text);
            }
        );

        public static readonly BindableProperty ErrorTextProperty = BindableProperty.Create(
            nameof(ErrorText),
            typeof(string),
            typeof(OutlinedMaterialEntry),
            default(string),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;

                view.errorTextEditor.Text = (string)newValue;
            }
        );

        public static readonly BindableProperty LeadingIconProperty = BindableProperty.Create(
            nameof(LeadingIcon),
            typeof(ImageSource),
            typeof(OutlinedMaterialEditor),
            default(ImageSource),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;

                view.leadingIconEditor.Source = (ImageSource)newValue;

                view.leadingIconEditor.IsVisible = !view.leadingIconEditor.Source.IsEmpty;
            }
        );

        public static readonly BindableProperty TrailingIconProperty = BindableProperty.Create(
            nameof(TrailingIcon),
            typeof(ImageSource),
            typeof(OutlinedMaterialEditor),
            default(ImageSource),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;

                view.trailingIconEditor.Source = (ImageSource)newValue;

                view.trailingIconEditor.IsVisible = view.trailingIconEditor.Source != null;
            }
        );

        public static readonly BindableProperty HasErrorProperty = BindableProperty.Create(
            nameof(HasError),
            typeof(bool),
            typeof(OutlinedMaterialEditor),
            default(bool),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;

                view.errorTextEditor.IsVisible = (bool)newValue;

                view.containerFrameEditor.BorderColor = view.errorTextEditor.IsVisible ? Color.Red : Color.Black;

                view.helperTextEditor.IsVisible = !view.errorTextEditor.IsVisible;

                view.placeholderTextEditor.TextColor = view.errorTextEditor.IsVisible ? Color.Red : Color.Gray;

                view.PlaceholderText = view.errorTextEditor.IsVisible ? $"{view.PlaceholderText}*" : view.PlaceholderText;

                if (view.TrailingIcon != null && !view.TrailingIcon.IsEmpty)
                    view.tempIcon = view.TrailingIcon;

                view.TrailingIcon = view.errorTextEditor.IsVisible
                    ? ImageSource.FromFile("ic_error.png")
                    : view.tempIcon;

                view.trailingIconEditor.IsVisible = view.errorTextEditor.IsVisible;
            }
        );

        

        public static readonly BindableProperty MaxLengthProperty = BindableProperty.Create(
            nameof(MaxLength),
            typeof(int),
            typeof(OutlinedMaterialEditor),
            default(int),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;

                view.customEditor.MaxLength = (int)newValue;

                view.charCounterTextEditor.IsVisible = view.customEditor.MaxLength > 0;

                view.charCounterTextEditor.Text = $"0 / {view.MaxLength}";
            }
        );

        public static readonly BindableProperty BorderColorProperty = BindableProperty.Create(
            nameof(BorderColor),
            typeof(Color),
            typeof(OutlinedMaterialEntry),
            Color.Blue,
            BindingMode.OneWay
        );

        /*
        public static readonly BindableProperty ReturnCommandProperty = BindableProperty.Create(
            nameof(ReturnCommand),
            typeof(ICommand),
            typeof(OutlinedMaterialEditor),
            default(ICommand),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialEditor)bindable;

                view.customEditor.ReturnCommand = (ICommand)newValue;
            }
        );*/

        public OutlinedMaterialEditor()
        {
            InitializeComponent();

            this.customEditor.Text = this.Text;

            this.customEditor.TextChanged += this.OnCustomEditorTextChanged;

            this.customEditor.Completed += this.OnCustomEditorCompleted;

        }



        public event EventHandler<EventArgs> EditorCompleted;

        public event EventHandler<TextChangedEventArgs> TextChanged;

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }


        public string PlaceholderText
        {
            get => (string)GetValue(PlaceholderTextProperty);
            set => SetValue(PlaceholderTextProperty, value);
        }

        public string HelperText
        {
            get => (string)GetValue(HelperTextProperty);
            set => SetValue(HelperTextProperty, value);
        }

        public string ErrorText
        {
            get => (string)GetValue(ErrorTextProperty);
            set => SetValue(ErrorTextProperty, value);
        }

        public ImageSource LeadingIcon
        {
            get => (ImageSource)GetValue(LeadingIconProperty);
            set => SetValue(LeadingIconProperty, value);
        }

        public ImageSource TrailingIcon
        {
            get => (ImageSource)GetValue(TrailingIconProperty);
            set => SetValue(TrailingIconProperty, value);
        }

        public bool HasError
        {
            get => (bool)GetValue(HasErrorProperty);
            set => SetValue(HasErrorProperty, value);
        }

        public int MaxLength
        {
            get => (int)GetValue(MaxLengthProperty);
            set => SetValue(MaxLengthProperty, value);
        }

        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(BorderColorProperty, value);
        }

        public Keyboard Keyboard
        {
            set => this.customEditor.Keyboard = value;
        }

        /*
        public ReturnType ReturnType
        {
            set => this.customEditor.ReturnType = value;
        }*/

        /*
        public ICommand ReturnCommand
        {
            get => (ICommand)GetValue(ReturnCommandProperty);
            set => SetValue(ReturnCommandProperty, value);
        }*/

        private async Task ControlFocused()
        {
            if (string.IsNullOrEmpty(this.customEditor.Text) || this.customEditor.Text.Length > 0)
            {
                this.customEditor.Focus();
                await this.ControlFocusedColor();
            }
            else
                await this.ControlUnfocused();
        }

        private async Task ControlFocusedColor()
        {
            this.containerFrameEditor.BorderColor = this.HasError ? Color.Red : this.BorderColor;
            this.placeholderTextEditor.TextColor = this.HasError ? Color.Red : this.BorderColor;

            int y = DeviceInfo.Platform == DevicePlatform.UWP ? -25 : -20;

            await this.placeholderContainerEditor.TranslateTo(0, y, 100, Easing.Linear);

            this.placeholderContainerEditor.HorizontalOptions = LayoutOptions.Start;
            this.placeholderTextEditor.FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label));

        }

        private async Task ControlUnfocused()
        {
            UnfocusedEditor();

            if (string.IsNullOrEmpty(this.customEditor.Text) || this.customEditor.MaxLength <= 0)
            {
                await UnfocusedEditorWithoutText();
            }
        }

        private async Task UnfocusedEditorWithoutText()
        {
            await this.placeholderContainerEditor.TranslateTo(0, 0, 100, Easing.Linear);
            this.placeholderContainerEditor.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.placeholderTextEditor.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));

        }

        private void UnfocusedEditor()
        {
            this.UnfocusedEditorColor();
            this.customEditor.Unfocus();
        }

        private void UnfocusedEditorColor()
        {
            this.containerFrameEditor.BorderColor = this.HasError ? Color.Red : Color.Black;
            this.placeholderTextEditor.TextColor = this.HasError ? Color.Red : Color.Gray;
        }

        private void CustomEditorFocused(object sender, FocusEventArgs e)
        {
            if (e.IsFocused)
                MainThread.BeginInvokeOnMainThread(async () => await this.ControlFocused());
        }

        private void CustomEditorUnfocused(object sender, FocusEventArgs e)
        {
            if (!e.IsFocused)
                MainThread.BeginInvokeOnMainThread(async () => await this.ControlUnfocused());
        }

        private void OutlinedMaterialEditorTapped(object sender, EventArgs e)
        {
            MainThread.BeginInvokeOnMainThread(async () => await this.ControlFocused());
        }

        private void OnCustomEditorTextChanged(object sender, TextChangedEventArgs e)
        {
            this.Text = e.NewTextValue;

            if (this.charCounterTextEditor.IsVisible)
                this.charCounterTextEditor.Text = $"{this.customEditor.Text.Length} / {this.MaxLength}";

            this.TextChanged?.Invoke(this, e);
        }
        private void OnCustomEditorCompleted(object sender, EventArgs e)
        {
            this.EditorCompleted?.Invoke(this, EventArgs.Empty);
        }
    }
}
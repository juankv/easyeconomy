﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Collections.ObjectModel;
using EasyEconomy.Models;

namespace EasyEconomy.Components
{
    public partial class OutlinedMaterialPicker :  Grid
    {
        private ImageSource tempIcon;

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
            nameof(ItemsSource),
            typeof(ObservableCollection<PickerValue>),
            typeof(OutlinedMaterialPicker),
            default(ObservableCollection<PickerValue>),
            BindingMode.TwoWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;
                view.customPicker.ItemsSource = (ObservableCollection<PickerValue>)newValue;
                var oldValueIn = (ObservableCollection<PickerValue>)oldValue;
                
            }
        );


        public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create(
            nameof(SelectedItem),
            typeof(PickerValue),
            typeof(OutlinedMaterialPicker),
            default(PickerValue),
            BindingMode.TwoWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;
                view.customPicker.SelectedItem = (PickerValue)newValue;
                var oldValueIn = (PickerValue)oldValue;
                if (oldValueIn == null && view.customPicker.SelectedIndex > -1)
                {
                    MainThread.BeginInvokeOnMainThread(async () => {
                        await view.ControlFocusedColor();
                        view.UnfocusedPickerColor();
                    });
                }
            }
        );

        public static readonly BindableProperty TitleProperty = BindableProperty.Create(
            nameof(Title),
            typeof(string),
            typeof(OutlinedMaterialPicker),
            default(string),
            BindingMode.TwoWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;
                view.customPicker.Title = (string)newValue;
            }
        );


        public static readonly BindableProperty PlaceholderTextProperty = BindableProperty.Create(
            nameof(PlaceholderText),
            typeof(string),
            typeof(OutlinedMaterialPicker),
            default(string),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;

                view.placeholderTextPicker.Text = (string)newValue;
            }
        );

       


        public static readonly BindableProperty HelperTextProperty = BindableProperty.Create(
            nameof(HelperText),
            typeof(string),
            typeof(OutlinedMaterialPicker),
            default(string),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;

                view.helperTextPicker.Text = (string)newValue;

                if (view.helperTextPicker.IsVisible)
                    view.helperTextPicker.IsVisible = false;
                else
                    view.helperTextPicker.IsVisible = !string.IsNullOrEmpty(view.helperTextPicker.Text);
            }
        );

        public static readonly BindableProperty ErrorTextProperty = BindableProperty.Create(
            nameof(ErrorText),
            typeof(string),
            typeof(OutlinedMaterialPicker),
            default(string),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;

                view.errorTextPicker.Text = (string)newValue;
            }
        );

        public static readonly BindableProperty LeadingIconProperty = BindableProperty.Create(
            nameof(LeadingIcon),
            typeof(ImageSource),
            typeof(OutlinedMaterialPicker),
            default(ImageSource),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;

                view.leadingIconPicker.Source = (ImageSource)newValue;

                view.leadingIconPicker.IsVisible = !view.leadingIconPicker.Source.IsEmpty;
            }
        );

        public static readonly BindableProperty TrailingIconProperty = BindableProperty.Create(
            nameof(TrailingIcon),
            typeof(ImageSource),
            typeof(OutlinedMaterialPicker),
            default(ImageSource),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;

                view.trailingIconPicker.Source = (ImageSource)newValue;

                view.trailingIconPicker.IsVisible = view.trailingIconPicker.Source != null;
            }
        );

        public static readonly BindableProperty HasErrorProperty = BindableProperty.Create(
            nameof(HasError),
            typeof(bool),
            typeof(OutlinedMaterialPicker),
            default(bool),
            BindingMode.OneWay,
            null,
            (bindable, oldValue, newValue) =>
            {
                var view = (OutlinedMaterialPicker)bindable;

                view.errorTextPicker.IsVisible = (bool)newValue;

                view.containerFramePicker.BorderColor = view.errorTextPicker.IsVisible ? Color.Red : Color.Black;

                view.helperTextPicker.IsVisible = !view.errorTextPicker.IsVisible;

                view.placeholderTextPicker.TextColor = view.errorTextPicker.IsVisible ? Color.Red : Color.Gray;

                view.PlaceholderText = view.errorTextPicker.IsVisible ? $"{view.PlaceholderText}*" : view.PlaceholderText;

                if (view.TrailingIcon != null && !view.TrailingIcon.IsEmpty)
                    view.tempIcon = view.TrailingIcon;

                view.TrailingIcon = view.errorTextPicker.IsVisible
                    ? ImageSource.FromFile("ic_error.png")
                    : view.tempIcon;

                view.trailingIconPicker.IsVisible = view.errorTextPicker.IsVisible;
            }
        );




        public static readonly BindableProperty BorderColorProperty = BindableProperty.Create(
            nameof(BorderColor),
            typeof(Color),
            typeof(OutlinedMaterialEntry),
            Color.Blue,
            BindingMode.OneWay
        );


        public OutlinedMaterialPicker()
        {
            InitializeComponent();

            this.customPicker.SelectedItem = this.SelectedItem;
            this.customPicker.ItemsSource = this.ItemsSource;
            this.customPicker.SelectedIndexChanged 
                += this.OnCustomPickerSelectedIndexChanged;
        }


        public event EventHandler<EventArgs> SelectedIndexChanged;

        public ObservableCollection<PickerValue> ItemsSource
        {
            get => (ObservableCollection<PickerValue>)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }


        public PickerValue SelectedItem
        {
            get => (PickerValue)GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }


        public string PlaceholderText
        {
            get => (string)GetValue(PlaceholderTextProperty);
            set => SetValue(PlaceholderTextProperty, value);
        }

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public string HelperText
        {
            get => (string)GetValue(HelperTextProperty);
            set => SetValue(HelperTextProperty, value);
        }

        public string ErrorText
        {
            get => (string)GetValue(ErrorTextProperty);
            set => SetValue(ErrorTextProperty, value);
        }

        public ImageSource LeadingIcon
        {
            get => (ImageSource)GetValue(LeadingIconProperty);
            set => SetValue(LeadingIconProperty, value);
        }

        public ImageSource TrailingIcon
        {
            get => (ImageSource)GetValue(TrailingIconProperty);
            set => SetValue(TrailingIconProperty, value);
        }

        public bool HasError
        {
            get => (bool)GetValue(HasErrorProperty);
            set => SetValue(HasErrorProperty, value);
        }


        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(BorderColorProperty, value);
        }
        private async Task ControlFocused()
        {
            if (this.customPicker.SelectedIndex == -1)
            {
                this.customPicker.Focus();
                await this.ControlFocusedColor();
            }
            else
                await this.ControlUnfocused();
        }

        private async Task ControlFocusedColor()
        {
            this.containerFramePicker.BorderColor = this.HasError ? Color.Red : this.BorderColor;
            this.placeholderTextPicker.TextColor = this.HasError ? Color.Red : this.BorderColor;

            int y = DeviceInfo.Platform == DevicePlatform.UWP ? -25 : -20;

            await this.placeholderContainerPicker.TranslateTo(0, y, 100, Easing.Linear);

            this.placeholderContainerPicker.HorizontalOptions = LayoutOptions.Start;
            this.placeholderTextPicker.FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label));

        }

        private async Task ControlUnfocused()
        {
            UnfocusedPicker();

            if (this.customPicker.SelectedIndex == -1)
            {
                await UnfocusedPickerWithoutText();
            }
        }

        private async Task UnfocusedPickerWithoutText()
        {
            await this.placeholderContainerPicker.TranslateTo(0, 0, 100, Easing.Linear);
            this.placeholderContainerPicker.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.placeholderTextPicker.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));

        }

        private void UnfocusedPicker()
        {
            this.UnfocusedPickerColor();
            this.customPicker.Unfocus();
        }

        private void UnfocusedPickerColor()
        {
            this.containerFramePicker.BorderColor = this.HasError ? Color.Red : Color.Black;
            this.placeholderTextPicker.TextColor = this.HasError ? Color.Red : Color.Gray;
        }

        private void CustomPickerFocused(object sender, FocusEventArgs e)
        {
            if (e.IsFocused)
                MainThread.BeginInvokeOnMainThread(async () => await this.ControlFocused());
        }

        private void CustomPickerUnfocused(object sender, FocusEventArgs e)
        {
            if (!e.IsFocused)
                MainThread.BeginInvokeOnMainThread(async () => await this.ControlUnfocused());
        }

        private void OutlinedMaterialPickerTapped(object sender, EventArgs e)
        {
            MainThread.BeginInvokeOnMainThread(async () => await this.ControlFocused());
        }

        private void OnCustomPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            var itemSelected = (Picker)sender;
            this.SelectedItem = (PickerValue)itemSelected.SelectedItem;
            this.SelectedIndexChanged?.Invoke(this, e);
        }
    }
}
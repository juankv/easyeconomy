﻿using EasyEconomy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Popups
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChartsPopupPage
    {
        ChartsViewModel _viewModel;
        public ChartsPopupPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ChartsViewModel();
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _viewModel.OnDisappearing();

        }
    }
}
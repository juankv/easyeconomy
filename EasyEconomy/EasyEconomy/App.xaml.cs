﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using EasyEconomy.Services.Storage;
using EasyEconomy.Views;
using EasyEconomy.Storage;
using Xamarin.Essentials;
using EasyEconomy.Services.Auth;
using EasyEconomy.Services;
using EasyEconomy.Helpers;

namespace EasyEconomy
{
    public partial class App : Application
    {
        public static DateTime DateCalendar;
        public static int EconomyId;
        public static String CategoryId;


        public App()
        {
            InitializeComponent();
            Init();
            RegisterServices();
            LoadStyles();
            MainPage = new AppShell();
        }

        private void Init()
        {
            EconomyId = 0;
            CategoryId = "";
            DateCalendar = DateTime.Now;
        }

        void LoadStyles()
        {
            if (DimensionResolution.IsASmallDevice())
            {
                dictionary.MergedDictionaries.Add(SmallDevicesStyle.SharedInstance);
            }
            else
            {
                dictionary.MergedDictionaries.Add(GeneralDevicesStyle.SharedInstance);
            }
        }

        private void RegisterServices()
        {
            DependencyService.Register<IFileHelper>();
            DependencyService.Register<ISQLite>();
            DependencyService.Register<IOAuth2Service>();
            DependencyService.Register<INativeFont>();
            DependencyService.Register<CategoryHeadStorage>();
            DependencyService.Register<EconomyDetailStorage>();
            DependencyService.Register<CategoryDetailStorage>();
            DependencyService.Register<EconomyHeadStorage>();
            DependencyService.Register<BusinessLogicQuery>();
            DependencyService.Register<TranslateExtension>();
        }

    }

    public class DimensionResolution
    {

        const int smallWightResolution = 768;
        const int smallHeightResolution = 1280;
        public static bool IsASmallDevice()
        {
            // Get Metrics
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;

            // Width (in pixels)
            var width = mainDisplayInfo.Width;

            // Height (in pixels)
            var height = mainDisplayInfo.Height;

            return (width <= smallWightResolution && height <= smallHeightResolution);
        }
    }
}

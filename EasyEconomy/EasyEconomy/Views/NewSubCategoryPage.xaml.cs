﻿using EasyEconomy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewSubCategoryPage : ContentPage
    {
        public NewSubCategoryPage()
        {
            InitializeComponent(); 
            BindingContext = new NewSubCategoryViewModel();
        }
    }
}
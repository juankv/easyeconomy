﻿using EasyEconomy.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateEconomyPage : ContentPage
    {
        UpdateEconomyViewModel _viewModel;
        public UpdateEconomyPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new UpdateEconomyViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
﻿using EasyEconomy.Models;
using EasyEconomy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{

    public partial class NewEconomyYearPage : ContentPage
    {
        public EconomyHead Item { get; set; }
        public NewEconomyYearPage()
        {
            InitializeComponent();
            BindingContext = new NewEconomyViewModel();
        }
    }
}
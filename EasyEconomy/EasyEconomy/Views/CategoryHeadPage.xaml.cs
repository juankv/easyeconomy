﻿using EasyEconomy.Models;
using EasyEconomy.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoryHeadPage : ContentPage
    {
        CategoryHeadViewModel _viewModel;
        public CategoryHeadPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new CategoryHeadViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
            _viewModel.IsBusy = false;
        }


        private async void EditButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as ImageButton;
            var item = button.CommandParameter as CategoryHeadView;
            await _viewModel.ExecuteEditCategoryCommand(item.CategoryHeadId);
        }

        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as ImageButton;
            var item = button.CommandParameter as CategoryHeadView;
            await _viewModel.ExecuteDeleteCategoryCommand(item.CategoryHeadId);
        }
    }
}
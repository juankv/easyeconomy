﻿using EasyEconomy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalendarEconomyPage : ContentPage
    {
        CalendarEconomyViewModel _viewModel;
        public CalendarEconomyPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new CalendarEconomyViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
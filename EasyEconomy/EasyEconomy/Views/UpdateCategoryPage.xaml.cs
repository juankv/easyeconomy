﻿using EasyEconomy.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateCategoryPage : ContentPage
    {
        public UpdateCategoryPage()
        {
            InitializeComponent();
            BindingContext  = new UpdateCategoryViewModel();
        }       
    }
}
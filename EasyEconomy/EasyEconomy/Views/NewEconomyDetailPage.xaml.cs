﻿using EasyEconomy.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using System.Threading.Tasks;
using EasyEconomy.Models;
using EasyEconomy.Components;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewEconomyDetailPage : ContentPage
    {
        NewEconomyDetailViewModel _viewModel;
        public NewEconomyDetailPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new NewEconomyDetailViewModel();


        }
        protected override void OnAppearing()
        {            
            base.OnAppearing();
            _viewModel.OnAppearing();                 
        }

        
    }
}
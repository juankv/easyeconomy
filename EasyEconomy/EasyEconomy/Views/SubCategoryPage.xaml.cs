﻿using EasyEconomy.Models;
using EasyEconomy.ViewModels;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SubCategoryPage : ContentPage
    {
        SubCategoryViewModel _viewModel;
        public SubCategoryPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new SubCategoryViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }


        private async void EditButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as ImageButton;
            var item = button.CommandParameter as SubCategoryView;
            await _viewModel.ExecuteEditSubCategoryCommand(item.CategoryDetailId);
        }

        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as ImageButton;
            var item = button.CommandParameter as SubCategoryView;
            await _viewModel.ExecuteDeleteSubCategoryCommand(item.CategoryDetailId);
        }


    }
}
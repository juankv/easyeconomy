﻿using EasyEconomy.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateEconomyDetailPage : ContentPage
    {
        UpdateEconomyDetailViewModel _viewModel;
        public UpdateEconomyDetailPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new UpdateEconomyDetailViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
﻿
using EasyEconomy.Services.Auth;
using EasyEconomy.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            this.BindingContext = new LoginViewModel();
        }
    }
}
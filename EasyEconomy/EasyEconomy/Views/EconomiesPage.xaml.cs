﻿using EasyEconomy.Models;
using EasyEconomy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    public partial class EconomiesPage : ContentPage
    {
        EconomyHeadViewModel _viewModel;
        public EconomiesPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new EconomyHeadViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing(); 
            _viewModel.OnAppearing();
        }

        private async void EditButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as ImageButton;
            var item = button.CommandParameter as EconomyHeadView;
            await _viewModel.ExecuteEditEconomyCommand(item.Id);
        }

        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as ImageButton;
            var item = button.CommandParameter as EconomyHeadView;
            await _viewModel.ExecuteDeleteEconomyCommand(item.Id);
        }
    }
}
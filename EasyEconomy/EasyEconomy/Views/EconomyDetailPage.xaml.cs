﻿using EasyEconomy.Models;
using EasyEconomy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EconomyDetailPage : ContentPage
    {
        EconomyDetailViewModel _viewModel;
        public EconomyDetailPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new EconomyDetailViewModel();
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }

        private async void EditButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as ImageButton;
            var item = button.CommandParameter as EconomyDetailView;
            await _viewModel.ExecuteEditEconomyDeatilCommand(item.IdEconomyHead, item.IdEconomyDetail);
        }


        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {
            var button = sender as ImageButton;
            var item = button.CommandParameter as EconomyDetailView;
            await _viewModel.ExecuteDeleteEconomyDeatilCommand(item);
        }
    }
}
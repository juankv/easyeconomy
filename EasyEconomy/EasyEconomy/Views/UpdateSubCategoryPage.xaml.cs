﻿using EasyEconomy.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyEconomy.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateSubCategoryPage : ContentPage
    {
        public UpdateSubCategoryPage()
        {
            InitializeComponent();
            BindingContext = new UpdateSubcategoryViewModel();
        }
    }
}
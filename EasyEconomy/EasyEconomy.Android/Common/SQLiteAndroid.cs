﻿using EasyEconomy.Droid.Common;
using EasyEconomy.Services.Storage;
using SQLite;
using System;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteAndroid))]
namespace EasyEconomy.Droid.Common
{
    public class SQLiteAndroid : ISQLite
    {
        #region ISQLite implementation
        public SQLiteAsyncConnection GetConnection()
        {
            var sqliteFilename = "EasyEconmySQLite";
            var path = DependencyService.Get<IFileHelper>().GetLocalFilePath(sqliteFilename) + ".db3";
            var conn = new SQLiteAsyncConnection(path, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.SharedCache);
            // Return the database connection 
            return conn;
        }

        public SQLiteAsyncConnection GetConnection(string dbName)
        {
            var sqliteFilename = dbName;
            var path = DependencyService.Get<IFileHelper>().GetLocalFilePath(sqliteFilename) + ".db3";
            var conn = new SQLiteAsyncConnection(path, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.SharedCache);
            // Return the database connection 
            return conn;
        }
        #endregion
    }
}
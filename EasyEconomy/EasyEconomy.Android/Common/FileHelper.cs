﻿using System;
using System.IO;
using EasyEconomy.Droid.Common;
using EasyEconomy.Services.Storage;

[assembly: Xamarin.Forms.Dependency(typeof(FileHelper))]
namespace EasyEconomy.Droid.Common
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            return System.IO.Path.Combine(path, filename);
        }

        public string GetLocalDocumentsPath(string filename)
        {
            string path = "/storage/emulated/0/documents/";
            return System.IO.Path.Combine(path, filename);
        }

        public void CreateDataBaseFromAssets()
        {
            var sqliteFilename = "EasyEconmySQLite";
            var path = GetLocalFilePath(sqliteFilename) + ".db3";
            using (BinaryReader br = new BinaryReader(Android.App.Application.Context.Assets.Open(sqliteFilename + ".db3")))
            {
                using (BinaryWriter bw = new BinaryWriter(new FileStream(path, FileMode.Create)))
                {
                    byte[] buffer = new byte[2048];
                    int len = 0;
                    while ((len = br.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        bw.Write(buffer, 0, len);
                    }
                }

            }

        }

        public string CreateDataBaseBackup()
        {
            var sqliteFilename = "EasyEconmySQLite";
            string nameFileBackup = sqliteFilename + "_" + DateTime.Now.ToString().Replace("/", "_").Replace(" ", "_").Replace(".", "").Replace(":", "_");
            var pathFileBackup1 = GetLocalDocumentsPath(nameFileBackup + ".db3");
            //var pathFileBackup2 = GetLocalDocumentsPath(nameFileBackup + ".db3-shm");
            //var pathFileBackup3 = GetLocalDocumentsPath(nameFileBackup + ".db3-wal");
            var pathApp1 = GetLocalFilePath(sqliteFilename) + ".db3";
            //var pathApp2 = GetLocalFilePath(sqliteFilename) + ".db3-shm";
            //var pathApp3 = GetLocalFilePath(sqliteFilename) + ".db3-wal";



            FileOverride(pathApp1, pathFileBackup1);
            //FileOverride(pathApp2, pathFileBackup2);
            //FileOverride(pathApp3, pathFileBackup3);

            return nameFileBackup;
        }

        public void RestoreDataBaseFromLastBackup()
        {
            var sqliteFilename = "EasyEconmySQLite";
            var pathFileBackup1 = GetLocalDocumentsPath("EasyEconmySQLite" + ".db3");
            //var pathFileBackup2 = GetLocalDocumentsPath(App.LastBackupName + ".db3-shm");
            //var pathFileBackup3 = GetLocalDocumentsPath(App.LastBackupName + ".db3-wal");
            var pathApp1 = GetLocalFilePath(sqliteFilename) + ".db3";
            //var pathApp2 = GetLocalFilePath(sqliteFilename) + ".db3-shm";
            //var pathApp3 = GetLocalFilePath(sqliteFilename) + ".db3-wal";

            FileOverride(pathApp1, pathFileBackup1);
            //FileOverride(pathApp2, pathFileBackup2);
            //FileOverride(pathApp3, pathFileBackup3);

        }

        public void FileOverride(string readFile, string writerFile)
        {
            try
            {
                using (BinaryReader br = new BinaryReader(new FileStream(readFile, FileMode.Open)))
                {
                    using (BinaryWriter bw = new BinaryWriter(new FileStream(writerFile, FileMode.Create)))
                    {
                        byte[] buffer = new byte[2048];
                        int len = 0;
                        while ((len = br.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            bw.Write(buffer, 0, len);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {

            }
        }
    }
}